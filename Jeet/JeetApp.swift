//
//  JeetApp.swift
//  Jeet
//
//  Created by Mac on 5/14/21.
//

import SwiftUI

@main
struct JeetApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
