//
//  strings.swift
//  Jeet
//
//  Created by Mac on 8/1/21.
//

import Foundation
extension String {
var fixedArabicURL: String?  {
       return self.addingPercentEncoding(withAllowedCharacters: CharacterSet.alphanumerics
           .union(CharacterSet.urlPathAllowed)
           .union(CharacterSet.urlHostAllowed))
   } }
