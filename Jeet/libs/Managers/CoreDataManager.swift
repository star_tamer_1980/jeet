//
//  CoreDataManager.swift
//  Jeet
//
//  Created by Mac on 6/10/21.
//

import Foundation
import CoreData

class CoreDataManager{
    let persistantContainer : NSPersistentContainer
    
    init(){
        persistantContainer = NSPersistentContainer(name: "cartData")
        persistantContainer.loadPersistentStores { (description, error) in
            if let error = error{
                fatalError("Core data store failed \(error.localizedDescription)")
            }
        }
    }
    
    
    func updateProductPluse(product: ProductListCoreModel, completion: @escaping ()->Void){
        let newProduct = ProductListCoreModel(context: persistantContainer.viewContext)
        newProduct.qty = product.qty + 1
        newProduct.id = Int16(product.id)
        newProduct.name = product.name
        newProduct.price = product.price
        newProduct.image = product.image
//        newProduct.setValue(product.qty + 1, forKey: "qty")
        print("from pluse \(product.qty)")
        do{
            try persistantContainer.viewContext.save()
        }catch{
            persistantContainer.viewContext.rollback()
            print("failed to pluse object \(error)")
        }
    }
    
    func updateProductMinuse(product: ProductListCoreModel, completion: @escaping ()->Void){
        product.qty = product.qty - 1
        product.setValue(product.qty - 1, forKey: "qty")
        print("from minuse \(product.qty)")
        do{
            try persistantContainer.viewContext.save()
        }catch{
            persistantContainer.viewContext.rollback()
            print("failed to minuse object \(error)")
        }
        completion()
    }
    
    func deleteProduct(product: ProductListCoreModel){
        persistantContainer.viewContext.delete(product)
        do{
            try persistantContainer.viewContext.save()
        }catch{
            persistantContainer.viewContext.rollback()
            print("failed to delete object \(error)")
        }
    }
    
    func getAllProductsCart() -> [ProductListCoreModel]{
        let fetchRequest: NSFetchRequest<ProductListCoreModel> = ProductListCoreModel.fetchRequest()
        do{
            return try persistantContainer.viewContext.fetch(fetchRequest)
        }catch{
            return []
        }
    }
    func addProduct(product: productDetailsModel, qty: Int){
        let newProduct = ProductListCoreModel(context: persistantContainer.viewContext)
        newProduct.id = Int16(product.id!)
        newProduct.name = product.name
        newProduct.price = Float(Double(product.price ?? "0.0")!)
        newProduct.qty = Int16(qty)
        newProduct.image = product.image
        
        do{
            try persistantContainer.viewContext.save()
        }catch{
            print(error.localizedDescription)
        }
    }
}
