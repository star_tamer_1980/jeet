//
//  mapView.swift
//  Jeet
//
//  Created by Mac on 6/20/21.
//

import Foundation
import SwiftUI
import MapKit

struct mapView: UIViewRepresentable {
    @ObservedObject var settings = UserSettings()
    @State var annotation = MKPointAnnotation()
    @Binding var latitude: String
    @Binding var longitude: String
    let map = MKMapView()
    func makeUIView(context: Context) -> some MKMapView {
        map.showsUserLocation = true
        map.delegate = context.coordinator
        
        return map
    }
    
    
    
    func makeCoordinator() -> Coordinator {
        Coordinator(self)
    }
    
    func updateUIView(_ uiView: UIViewType, context: Context) {
        uiView.removeAnnotations(uiView.annotations)
        uiView.addAnnotation(annotation)
    }
    
    func addAnnotation(for coordinate: CLLocationCoordinate2D) {
            let newAnnotation = MKPointAnnotation()
            newAnnotation.coordinate = coordinate
            annotation = newAnnotation
        latitude = String(annotation.coordinate.latitude)
        longitude = String(annotation.coordinate.longitude)
        
    }
    
}


