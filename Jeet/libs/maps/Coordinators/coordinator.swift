//
//  coordinator.swift
//  Jeet
//
//  Created by Mac on 6/20/21.
//

import Foundation
import MapKit

final class Coordinator: NSObject, MKMapViewDelegate, UIGestureRecognizerDelegate{
    
    var gRecognizer = UITapGestureRecognizer()
    
    var control: mapView
    init(_ control: mapView){
        self.control = control
        
        super.init()
        self.gRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapHandler))
        self.gRecognizer.delegate = self
        self.control.map.addGestureRecognizer(gRecognizer)
        
    }
    @objc func tapHandler(_ gesture: UITapGestureRecognizer) {
            // position on the screen, CGPoint
        let location = gRecognizer.location(in: self.control.map)
            // position on the map, CLLocationCoordinate2D
        let coordinate = self.control.map.convert(location, toCoordinateFrom: self.control.map)
        self.control.addAnnotation(for: coordinate)
        print("test to handle gesture coordinate lat \(coordinate.latitude) long \(coordinate.longitude)")
            
        }
}
