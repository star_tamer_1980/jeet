//
//  test.swift
//  Jeet
//
//  Created by Mac on 5/26/21.
//


/*
 
 "code": "woocommerce_rest_authentication_error",
 "message": "التوقيع غير صالح - التوقيع المقدم غير متطابق.",
 "data": {
     "status": 401
 }
}
 */
import Foundation

struct ErrorData: Decodable{
    var code: String
    var message: String
    var data: data
}

struct data: Decodable {
    var status: Int
}
