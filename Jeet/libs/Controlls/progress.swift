//
//  progress.swift
//  Jeet
//
//  Created by Mac on 6/7/21.
//

import SwiftUI

struct progress: View {
    var body: some View {
        ProgressView()
            .progressViewStyle(CircularProgressViewStyle(tint: colorPallet.move))
            .scaleEffect(2)
    }
}

struct progress_Previews: PreviewProvider {
    static var previews: some View {
        progress()
    }
}
