//
//  viewEX.swift
//  Jeet
//
//  Created by Mac on 6/4/21.
//

import SwiftUI

extension View {
    func Print(_ vars: Any...) -> some View {
        for v in vars { print(v) }
        return EmptyView()
    }
}
