//
//  chooseFont.swift
//  Jeet
//
//  Created by Mac on 7/23/21.
//

import Foundation
import SwiftUI

struct ChooseFontModifier: ViewModifier {
    var setFontManager: fontManager
    var setStyle: colorManager
    var setWeight: Font.Weight = .regular
    var setSize: Int = 12
    
    var setTextColor: Color {
        switch setStyle {
        case .black:
            return colorPallet.black
        case .white:
            return colorPallet.white
        case .move:
            return colorPallet.move
        case .orange:
            return colorPallet.orange
        case .gray:
            return colorPallet.gray
        case .bgTextField:
            return colorPallet.bgTextField
        case .buttonFacebookBlue:
            return colorPallet.buttonFacebookBlue
        case .buttonGoogleRed:
            return colorPallet.buttonGoogleRed
        default:
            return Color.init("blackColor")
        }
    }
    
    var setTextFont: String{
        switch setFontManager{
        case .JeetBlack:
            return fontPallet.JeetBlack
        case .JeetBold:
            return fontPallet.JeetBold
        case .JeetLight:
            return fontPallet.JeetLight
        case .JeetPlain:
            return fontPallet.JeetPlain
        case .JeetSemiBold:
            return fontPallet.JeetSemiBold
        case .JeetSemiLight:
            return fontPallet.JeetSemiLight
        case .JeetExtraBold:
            return fontPallet.JeetExtraBold
        case .JeetExtraLight:
            return fontPallet.JeetExtraLight
        case .monadi:
            return fontPallet.monadi
        case .BalooBhaijaan:
            return fontPallet.BalooBhaijaan
        case .BoutrosAsma:
            return fontPallet.BoutrosAsma
        case .BoutrosNewsH1:
            return fontPallet.BoutrosNewsH1
        }
    }

    func body(content: Content) -> some View {
        content
        .font(Font.custom(setTextFont, size: CGFloat(setSize))
        .weight(setWeight))
        .foregroundColor(setTextColor)
    }
    
}
