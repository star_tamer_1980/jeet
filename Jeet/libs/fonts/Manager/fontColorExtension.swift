//
//  fontColorExtension.swift
//  Jeet
//
//  Created by Mac on 7/23/21.
//
import Foundation
import SwiftUI


struct colorPallet{
    static let move = Color("indecatorActive")
    static let black = Color("blackColor")
    static let white = Color("whiteColor")
    static let orange = Color("orange")
    static let bgCard = Color("bgCard")
    static let blueLight = Color("blueLight")
    static let bgTextField = Color("bgTextField")
    static let buttonFacebookBlue = Color("buttonFacebookBlue")
    static let buttonGoogleRed = Color("buttonGoogleRed")
    static let gray = Color.gray
}

enum colorManager {
    case move
    case black
    case white
    case orange
    case bgCard
    case blueLight
    case bgTextField
    case buttonFacebookBlue
    case buttonGoogleRed
    case gray
}


