//
//  fontType.swift
//  Jeet
//
//  Created by Mac on 7/23/21.
//

import SwiftUI

struct fontPallet{
    static let JeetBlack = "JeetBlack"
    static let JeetBold = "JeetBold"
    static let JeetExtraBold = "JeetExtraBold"
    static let JeetExtraLight = "JeetExtraLight"
    static let JeetLight = "JeetLight"
    static let JeetPlain = "JeetPlain"
    static let JeetSemiBold = "JeetSemiBold"
    static let JeetSemiLight = "JeetSemiLight"
    static let monadi = "Monadi"
    static let BalooBhaijaan = "BalooBhaijaan-Regular"
    static let BoutrosAsma = "boutrosAsma"
    static let BoutrosNewsH1 = "BoutrosNewsH1-Bold"
}

enum fontManager {
    case JeetBlack
    case JeetBold
    case JeetExtraBold
    case JeetExtraLight
    case JeetLight
    case JeetPlain
    case JeetSemiBold
    case JeetSemiLight
    case monadi
    case BalooBhaijaan
    case BoutrosAsma
    case BoutrosNewsH1
}

