//
//  FontExtension.swift
//  Jeet
//
//  Created by Mac on 5/18/21.
//

import SwiftUI
extension View {
    func chooseFont(fontManager: fontManager, style: colorManager, weight: Font.Weight, size: Int) -> some View {
        self.modifier(ChooseFontModifier(setFontManager: fontManager, setStyle: style, setWeight: weight, setSize: size))
    }
    
    
    
    func MonadiFont(style: colorManager, weight: Font.Weight, size: Int) -> some View {
        self.modifier(MonadiFontModifier(style: style, weight: weight, size: size))
    }
    func balooFont(style: colorManager, weight: Font.Weight, size: Int) -> some View {
        self.modifier(BalooFontModifier(style: style, weight: weight, size: size))
    }
    func boutrosAsmaFont(style: colorManager, weight: Font.Weight, size: Int) -> some View {
        self.modifier(BoutrosAsmaFontModifier(style: style, weight: weight, size: size))
    }
    func boutrosBoldFont(style: colorManager, weight: Font.Weight, size: Int) -> some View {
        self.modifier(BoutrosBoldFontModifier(style: style, weight: weight, size: size))
    }
}
