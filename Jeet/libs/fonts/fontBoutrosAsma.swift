//
//  fontBoutrosAsma.swift
//  Jeet
//
//  Created by Mac on 5/18/21.
//

import Foundation
import SwiftUI

struct BoutrosAsmaFontModifier: ViewModifier {
    
    var style: colorManager
    var weight: Font.Weight = .regular
    var size: Int = 12
    
    var textColor: Color {
        switch style {
        case .black:
            return colorPallet.black
        case .white:
            return colorPallet.white
        case .move:
            return colorPallet.move
        case .orange:
            return colorPallet.orange
        case .gray:
            return colorPallet.gray
        case .bgTextField:
            return colorPallet.bgTextField
        case .buttonFacebookBlue:
            return colorPallet.buttonFacebookBlue
        case .buttonGoogleRed:
            return colorPallet.buttonGoogleRed
        default:
            return Color.init("blackColor")
        }
    }


    func body(content: Content) -> some View {
        content
        .font(Font.custom("boutrosAsma", size: CGFloat(size))
        .weight(weight))
        .foregroundColor(textColor)
    }
    
}
