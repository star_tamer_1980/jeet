//
//  categoryCard.swift
//  Jeet
//
//  Created by Mac on 6/1/21.
//

import SwiftUI
import Kingfisher


struct categoryCard: View{
    var row = catShort()
    var isLast = false
    var isLoadMore = true
    var body: some View{
        VStack(alignment: .center, spacing: 10){
            Text(verbatim: row.name!)
                .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 10)
                .multilineTextAlignment(.center)
            
            
            if(isLast == true && isLoadMore == true){
                VStack(alignment: .leading, spacing: 15){
                    ProgressView()
                }
                
            }
        }
        .padding()
        .shadow(color: colorPallet.gray, radius: 3, x: 2, y: 2)
    }
}
