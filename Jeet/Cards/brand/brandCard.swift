//
//  brandCard.swift
//  Jeet
//
//  Created by Mac on 6/1/21.
//

import SwiftUI
import Kingfisher


struct brandCard: View{
    var row = catShort()
    var isLast = false
    var isLoadMore = true
    var body: some View{
        VStack(alignment: .center, spacing: 10){
            KFImage(URL(string: (row.image != "") ? row.image! : "https://jeet.codlop.com/wp-content/uploads/2021/05/a-564-300x300.jpg"))
                .resizable()
                .scaledToFit()
            
            if(isLast == true && isLoadMore == true){
                VStack(alignment: .leading, spacing: 15){
                    ProgressView()
                }
                
            }
        }
        .padding()
        .shadow(color: colorPallet.gray, radius: 3, x: 2, y: 2)
    }
}
