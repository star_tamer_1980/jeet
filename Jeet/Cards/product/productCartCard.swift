//
//  productCartCard.swift
//  Jeet
//
//  Created by Mac on 6/11/21.
//

import SwiftUI
import Kingfisher

struct productCartCard: View {
    @Binding var product : ProductListCoreModel
    let coreDM = CoreDataManager()
    var body: some View{
        VStack(alignment: .center, spacing: 10){
            HStack{
                VStack{
                    KFImage(URL(string: product.image ?? ""))
                        .resizable()
                        .frame(width: 40, height: 40, alignment: .leading)
                }
                .frame(width: 50, height: 50)
                .background(colorPallet.blueLight)
                VStack(alignment: .leading, spacing: 5){
                    Text(verbatim: product.name ?? "")
                        .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 15)
                        .multilineTextAlignment(.leading)
                    HStack{
                        VStack{
                            Text("السعر")
                                .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 15)
                            Text(String(product.price))
                                .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 15)
                        }
                        
                        Spacer()
                        VStack{
                            Text("العدد")
                                .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 15)
                            Text(String(product.qty))
                                .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 15)
                        }
                        Spacer()
                        VStack{
                            Text("السعر الاجمالي")
                                .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 15)
                            Text(String(Float(product.qty) * product.price))
                                .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 15)
                        }
                    }
                    
                }
                
            }
            
                
        }
        .padding()
        .shadow(color: colorPallet.gray, radius: 3, x: 2, y: 2)
        .background(colorPallet.bgCard)
    }
}

//struct productCartCard_Previews: PreviewProvider {
//    static var previews: some View {
//        productCartCard()
//    }
//}
