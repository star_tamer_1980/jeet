//
//  productCard.swift
//  Jeet
//
//  Created by Mac on 6/1/21.
//

import SwiftUI
import Kingfisher


struct productCard: View{
    var product = productShort()
    var isLast = false
    var isLoadMore = true
    var body: some View{
        VStack(alignment: .center, spacing: 10){
            KFImage(URL(string: product.image!))
                .resizable()
                .scaledToFit()
            Text(verbatim: product.name!)
                .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 10)
                .multilineTextAlignment(.center)
                
                
            HStack(alignment: .top, content: {
                
                Text(product.regular_price!)
                    .strikethrough(true, color: colorPallet.gray)
                    .chooseFont(fontManager: .BalooBhaijaan, style: .gray, weight: .medium, size: 8)
                Spacer()
                Text("\(product.price!)")
                    .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 12)
            })
            HStack(alignment: .top, spacing: 1){
                Text("(\(NSString(format: "%.2f", product.average_rating ?? 0)))")
                    .MonadiFont(style: .orange, weight: .medium, size: 12)
                Spacer()
                StarsView(rating: CGFloat(product.average_rating ?? 0), maxRating: 5)
                    .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 12)
            }
            if(isLast == true && isLoadMore == true){
                VStack(alignment: .leading, spacing: 15){
                    progress()
                }
                
            }
        }
        .padding()
        .shadow(color: colorPallet.gray, radius: 3, x: 2, y: 2)
        .background(colorPallet.bgCard)
    }
}
