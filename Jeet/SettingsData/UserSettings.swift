//
//  UserSettings.swift
//  Jeet
//
//  Created by Mac on 5/19/21.
//

import Foundation
import Combine


final class UserSettings: ObservableObject {

    let objectWillChange = PassthroughSubject<Void, Never>()

    @UserDefault("ShowOnStart", defaultValue: true)
    var showOnStart: Bool {
        willSet {
            objectWillChange.send()
        }
    }
    
    
    @UserDefault("isLogin", defaultValue: false)
    var isLogin: Bool {
        willSet {
            objectWillChange.send()
        }
    }
    
    
    @UserDefault("token", defaultValue: "")
    var token: String {
        willSet {
            objectWillChange.send()
        }
    }
    
    @UserDefault("firstName", defaultValue: "")
    var firstName: String {
        willSet {
            objectWillChange.send()
        }
    }
    
    @UserDefault("lastName", defaultValue: "")
    var lastName: String {
        willSet {
            objectWillChange.send()
        }
    }
    
    @UserDefault("username", defaultValue: "")
    var username: String {
        willSet {
            objectWillChange.send()
        }
    }
    
    @UserDefault("email", defaultValue: "")
    var email: String {
        willSet {
            objectWillChange.send()
        }
    }
    
    @UserDefault("phone", defaultValue: "")
    var phone: String {
        willSet {
            objectWillChange.send()
        }
    }
    
    @UserDefault("phoneVerify", defaultValue: false)
    var phoneVerify: Bool {
        willSet {
            objectWillChange.send()
        }
    }
    
    @UserDefault("nicename", defaultValue: "")
    var nicename: String {
        willSet {
            objectWillChange.send()
        }
    }
    
    @UserDefault("displayName", defaultValue: "")
    var displayName: String {
        willSet {
            objectWillChange.send()
        }
    }
    
    @UserDefault("addressLatitude", defaultValue: 0.0)
    var addressLatitude: Double {
        willSet {
            objectWillChange.send()
        }
    }
    
    @UserDefault("addressLongitude", defaultValue: 0.0)
    var addressLongitude: Double {
        willSet {
            objectWillChange.send()
        }
    }
    
    @UserDefault("addressFName", defaultValue: "")
    var addressFName: String {
        willSet {
            objectWillChange.send()
        }
    }
    
    @UserDefault("addressLName", defaultValue: "")
    var addressLName: String {
        willSet {
            objectWillChange.send()
        }
    }
    
    @UserDefault("addressTitle", defaultValue: "")
    var addressTitle: String {
        willSet {
            objectWillChange.send()
        }
    }
    
    @UserDefault("addressCity", defaultValue: "")
    var addressCity: String {
        willSet {
            objectWillChange.send()
        }
    }
    
    @UserDefault("addressGovernorateId", defaultValue: "")
    var addressGovernorateId: String {
        willSet {
            objectWillChange.send()
        }
    }
    
    @UserDefault("addressGovernorateName", defaultValue: "")
    var addressGovernorateName: String {
        willSet {
            objectWillChange.send()
        }
    }
    
    @UserDefault("addressState", defaultValue: "")
    var addressState: String {
        willSet {
            objectWillChange.send()
        }
    }
    
    @UserDefault("addressPostCode", defaultValue: "")
    var addressPostCode: String {
        willSet {
            objectWillChange.send()
        }
    }
    
    @UserDefault("addressCountry", defaultValue: "")
    var addressCountry: String {
        willSet {
            objectWillChange.send()
        }
    }
    
    @UserDefault("addressPlaceId", defaultValue: "")
    var addressPlaceId: String {
        willSet {
            objectWillChange.send()
        }
    }
}
