//
//  shippingModel.swift
//  Jeet
//
//  Created by Mac on 6/14/21.
//

import SwiftUI

struct shippingModel: Decodable, Hashable{
    var canShip: Bool?
    var totalItems: Int?
    var cost: totalShippingModel?
    var address: addressShippingModel?
    
    enum CodingKeys: String, CodingKey{
        case canShip = "can_ship"
        case totalItems = "total_items"
    }
}
struct totalShippingModel: Decodable, Hashable{
    var subtotal: Int?
    var shippingTotal: Int?
    var total: Int?
    
    enum CodingKeys: String, CodingKey{
        case subtotal = "subtotal"
        case shippingTotal = "shipping_total"
        case total = "total"
    }
}
struct addressShippingModel: Decodable, Hashable{
    var billingGovernorate: String?
    var billingFirstName: String?
    var billingLastName: String?
    var billingCountry: String?
    var billingAddress1: String?
    var billingCity: String?
    var billingState: String?
    var billingPostcode: String?
    var billingEmail: String?
    var billingAddressLat: String?
    var billingAddressLng: String?
    var billingAddressPlaceId: String?
    
    enum CodingKeys: String, CodingKey{
        case billingGovernorate = "billing_governorate"
        case billingFirstName = "billing_first_name"
        case billingLastName = "billing_last_name"
        case billingCountry = "billing_country"
        case billingAddress1 = "billing_address_1"
        case billingCity = "billing_city"
        case billingState = "billing_state"
        case billingPostcode = "billing_postcode"
        case billingEmail = "billing_email"
        case billingAddressLat = "billing_address_lat"
        case billingAddressLng = "billing_address_lng"
        case billingAddressPlaceId = "billing_address_place_id"
    }
}
