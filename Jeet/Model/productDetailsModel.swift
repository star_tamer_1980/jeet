//
//  productDetailsModel.swift
//  Jeet
//
//  Created by Mac on 6/4/21.
//

import SwiftUI

struct productDetailsModel: Decodable, Hashable{
    var id: Int?
    var name: String?
    var regular_price: String?
    var price: String?
    var image: String?
    var featured: Bool?
    var date_created: String?
    var reviews_allowed: Bool?
    var reviews_count: Int?
    var average_rating: Int?
    var quantity: Int?
    var store_name: String?
    var ship_to_title: String?
    var ship_to_list: [shipToModel]?
    var rating_count: ratingCountModel?
    var attributes: [attributesModel]?
}

struct ratingCountModel: Decodable, Hashable{
    var first: Int?
    var second: Int?
    var third: Int?
    var fourth: Int?
    var fifth: Int?
    enum CodingKeys: String, CodingKey{
        case first  = "1"
        case second = "2"
        case third  = "3"
        case fourth = "4"
        case fifth  = "5"
    }
}

struct attributesModel: Decodable, Hashable {
    var id: Int?
    var taxonomy: String?
    var name: String?
    var values = Array<String>()
    
}
struct shipToModel: Decodable, Hashable {
    var id: Int?
    var city: String?
    
}

