//
//  alertResultModel.swift
//  Jeet
//
//  Created by Mac on 6/19/21.
//

import Foundation
struct alertResultModel: Decodable, Hashable{
    var status: Bool?
    var message: String?
}
