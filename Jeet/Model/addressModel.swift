//
//  addressModel.swift
//  Jeet
//
//  Created by Mac on 7/3/21.
//

import Foundation
struct addressModel: Decodable, Hashable{
    var postal_code: String?
    var country: String?
    var country_code: String?
    var city: String?
    var state: String?
    var plus_code: String?
    var place_id: String?
    var address: String?
    var location: coordinateModel?
}

struct coordinateModel: Decodable, Hashable{
    var latitude: Double?
    var longitude: Double?
}
