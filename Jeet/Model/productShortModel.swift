//
//  productShortModel.swift
//  Jeet
//
//  Created by Mac on 5/26/21.
//

import Foundation

struct productShort: Decodable, Hashable{
    var id: Int?
    var name: String?
    var regular_price: String?
    var price: String?
    var image: String?
    var in_cart: Bool?
    var in_favorite: Bool?
    var average_rating: Float?
    var featured: Bool?
    

}
