//
//  productCartCoreModel.swift
//  Jeet
//
//  Created by Mac on 6/10/21.
//

import SwiftUI
import CoreData

struct productCartCoreModel: Identifiable {
     var id = 0
     var name = ""
     var price = 0.0
     var qty = 0
     var image = ""
     var store_name = ""
    
}
