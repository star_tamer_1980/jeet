//
//  userModel.swift
//  Jeet
//
//  Created by Mac on 6/19/21.
//


import SwiftUI

struct userModel: Decodable, Hashable{
    var id: Int?
    var fName: String?
    var lName: String?
    var niceName: String?
    var displayName: String?
    var email: String?
    var username: String?
    var phone: String?
    var password: String?
    var token: String?
    var isLogin: Bool? = false
    var phoneVerify: Bool? = false
    var address : userAddress?
    
    enum CodingKeys: String, CodingKey{
        case id
        case fName = "first_name"
        case lName = "last_name"
        case niceName = "user_nicename"
        case displayName = "user_display_name"
        case email
        case username
        case phone
        case password
        case token
        case isLogin
    }
}


struct userAddress: Decodable, Hashable{
    var fName: String?
    var lName: String?
    var title: String?
    var city: String?
    var governorateId: String?
    var governorateName: String?
    var state: String?
    var postcode: String?
    var country: String?
    var latitude: String?
    var longitude: String?
    var placeId: String?
    
    enum CodingKeys: String, CodingKey{
        case fName = "billing_first_name"
        case lName = "billing_last_name"
        case title = "billing_address_1"
        case city = "billing_city"
        case governorateId = "billing_governorate"
        case governorateName = "billing_governorate_name"
        case state = "billing_state"
        case postcode = "billing_postcode"
        case country = "billing_country"
        case latitude = "billing_latitude"
        case longitude = "billing_longitude"
        case placeId = "billing_place_id"
    }
}

