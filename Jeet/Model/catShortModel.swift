//
//  catShortModel.swift
//  Jeet
//
//  Created by Mac on 6/1/21.
//

import SwiftUI

struct catShort: Decodable, Hashable{
    var id: Int?
    var name: String?
    var image: String?
    var description: String?
    var parent: Int?
    var total_products: Int?
}
