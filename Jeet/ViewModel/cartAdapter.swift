//
//  cartAdapter.swift
//  Jeet
//
//  Created by Mac on 6/14/21.
//

import SwiftUI
import Alamofire
import SwiftyJSON

class cartObserval: API, ObservableObject{
    
    let url = APIURL.baseUrl + "wp-json/jeet/user/update/cart"
    
    
    
    @Published var list = [paymentGatwayModel]()
    @Published var data = shippingModel()
    
    func getShippingData(token: String?, products: String? = "", completion: @escaping ()->Void) {
        let headers : HTTPHeaders = [
            "Authorization": "Bearer " + token!
        ]
        var parameters : [String:Any] = [:]
        if (products != ""){
            parameters["products"] = products
        }
        
        
        AF.request(url, method: .post, parameters: parameters, encoding: URLEncoding.httpBody, headers: headers).responseJSON{ response in
                    switch response.result {
                    case .success(let value):
                            let json = JSON(value)
                            print(value)
                            guard let gatewaysArr = json["gateways"].array else{
                                print("error from json")
                                completion()
                                return
                            }
        
                            for data in gatewaysArr{
                                guard let data = data.dictionary else {return}
                                print("title \(data["title"]?.stringValue ?? "")")
                                let row = paymentGatwayModel(id: data["id"]?.stringValue ?? "code", title: data["title"]?.stringValue ?? "", description: data["description"]?.stringValue ?? "", icon: data["icon"]?.stringValue ?? "")
                                    //productShort(id: data["id"]?.intValue ?? 0, name: data["name"]?.stringValue ?? "", regular_price: data["regular_price"]?.stringValue ?? "", price: data["price"]?.stringValue ?? "", image: data["image"]?.stringValue ?? "", in_cart: data["in_cart"]?.boolValue ?? false, in_favorite: data["in_favorite"]?.boolValue ?? false, average_rating: data["average_rating"]?.floatValue ?? 0.0, featured: data["featured"]?.boolValue ?? false)
                                self.list.append(row)
                            }
                        self.data.canShip = json["can_ship"].boolValue
                        self.data.totalItems = json["total_items"].intValue
                        self.data.cost?.subtotal = json["totals"]["subtotal"].intValue
                        self.data.cost?.total = json["totals"]["total"].intValue
                        self.data.cost?.shippingTotal = json["totals"]["shipping_total"].intValue
                        var address = addressShippingModel()
                        address.billingGovernorate = json["address"]["billing_governorate"].stringValue
                        address.billingFirstName = json["address"]["billing_first_name"].stringValue
                        address.billingLastName = json["address"]["billing_last_name"].stringValue
                        address.billingCountry = json["address"]["billing_country"].stringValue
                        address.billingCity = json["address"]["billing_city"].stringValue
                        address.billingAddress1 = json["address"]["billing_address_1"].stringValue
                        address.billingState = json["address"]["billing_state"].stringValue
                        address.billingPostcode = json["address"]["billing_postcode"].stringValue
                        address.billingEmail = json["address"]["billing_email"].stringValue
                        address.billingAddressLat = json["address"]["billing_address_lat"].stringValue
                        address.billingAddressLng = json["address"]["billing_address_lng"].stringValue
                        address.billingAddressPlaceId = json["address"]["billing_address_place_id"].stringValue
                        self.data.address = address
                        completion()
        
                    case .failure(let error):
                        print("error \(error)")
                        print(error.errorDescription)
                        completion()
                    }
                }
    
    }
    
    
}

