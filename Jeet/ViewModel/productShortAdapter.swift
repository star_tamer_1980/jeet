//
//  productShortAdapter.swift
//  Jeet
//
//  Created by Mac on 6/5/21.
//

import SwiftUI
import Alamofire
import SwiftyJSON

class productsShortListObserval: API, ObservableObject{
    
    // Start pagination -----------------------------------

    // get data now
    var isLoading = false
    
    // is found new data or get all data in server?
    var isMore = true
    
    // End pagination -------------------------------------
    
    let url = APIURL.baseUrl + "wp-json/jeet/get/products"
    let headers : HTTPHeaders = []
    
    @Published var list = [productShort]()
    
    
    
    func getList(limit: Int? = 10, paged: Int? = 1, order: String? = "", orderby: String? = "", category_id: Int? = 0, taxonomy: String? = "", searchWord: String? = "", completion: @escaping ()->Void) {
        
        var parameters : [String:Any] = [:]
        if (limit != 0){
            parameters["limit"] = limit
        }
        if (paged != 0){
            parameters["paged"] = paged
        }
        if (order != ""){
            parameters["order"] = order
        }
        if (orderby != ""){
            parameters["orderby"] = orderby
        }
        if (category_id != 0){
            parameters["category_id"] = category_id
        }
        if (taxonomy != ""){
            parameters["taxonomy"] = taxonomy
        }
        if (searchWord != ""){
            parameters["search"] = searchWord
        }
        if(self.isLoading){
            return
        }
        if(self.isMore == false){
            return
        }
        self.isLoading = true
        print("searchword data = \(searchWord)")
        AF.request(url, method: .post, parameters: parameters, encoding: URLEncoding.httpBody, headers: headers).responseJSON{ response in
                    switch response.result {
                    case .success(let value):
                            let json = JSON(value)
//                            print(value)
                            guard let dataArr = json["products"].array else{
                                print("error from json")
                                completion()
                                return
                            }
        
                            for data in dataArr{
                                guard let data = data.dictionary else {return}
                                
                                let row = productShort(id: data["id"]?.intValue ?? 0, name: data["name"]?.stringValue ?? "", regular_price: data["regular_price"]?.stringValue ?? "", price: data["price"]?.stringValue ?? "", image: (data["image"]?.stringValue ?? "").fixedArabicURL!, in_cart: data["in_cart"]?.boolValue ?? false, in_favorite: data["in_favorite"]?.boolValue ?? false, average_rating: data["average_rating"]?.floatValue ?? 0.0, featured: data["featured"]?.boolValue ?? false)
                                self.list.append(row)
                            }
                        self.EndPagination(limitList: json["total"].intValue, countList: self.list.count)
                        completion()
        
                    case .failure(let error):
                        print("error \(error)")
                        print(error.errorDescription)
                        completion()
                    }
                }
    }
    private func EndPagination(limitList: Int?, countList: Int?){
        self.isLoading = false
        if(limitList == countList){
            self.isMore = false
        }
    }
}
