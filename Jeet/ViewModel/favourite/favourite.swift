//
//  favourite.swift
//  Jeet
//
//  Created by Mac on 7/31/21.
//

import Foundation
import Alamofire
import SwiftyJSON

class favouriteObservable: API, ObservableObject{
    @Published var listFav = [productShort]()
    func listFavourite(_ token: String){
        let headers : HTTPHeaders = []
        let url = APIURL.baseUrl + "wp-json/jeet/user/get/favorite"
        var parameter : [String: Any] = [:]
        parameter["oauth_token"] = token
        
        AF.request(url, method: .post, parameters: parameter, encoding: URLEncoding.httpBody, headers: headers).responseJSON{ response in
            switch response.result {
            case .success(let value):
                    let json = JSON(value)
                            print(value)
                    guard let dataArr = json["products"].array else{
                        print("error from json")
                        return
                    }

                    for data in dataArr{
                        guard let data = data.dictionary else {return}
                        
                        let row = productShort(id: data["id"]?.intValue ?? 0, name: data["name"]?.stringValue ?? "", regular_price: data["regular_price"]?.stringValue ?? "", price: data["price"]?.stringValue ?? "", image: (data["image"]?.stringValue ?? "").fixedArabicURL!, in_cart: data["in_cart"]?.boolValue ?? false, in_favorite: data["in_favorite"]?.boolValue ?? false, average_rating: data["average_rating"]?.floatValue ?? 0.0, featured: data["featured"]?.boolValue ?? false)
                        self.listFav.append(row)
                    }
                return

            case .failure(let error):
                print("error \(error)")
                print(error.errorDescription)
                return
            }
        }
    }
}


