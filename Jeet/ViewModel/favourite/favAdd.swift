//
//  favAdd.swift
//  Jeet
//
//  Created by Mac on 7/31/21.
//

import Foundation
import Alamofire
import SwiftyJSON

extension favouriteObservable{
    
    
    func addToFavourite(_ token: String, _ productId: Int){
        let headers : HTTPHeaders = []
        let url = APIURL.baseUrl + "wp-json/jeet/user/add/favorite"
        var parameter : [String: Any] = [:]
        parameter["oauth_token"] = token
        parameter["product_id"] = productId
        
        AF.request(url, method: .post, parameters: parameter, encoding: URLEncoding.httpBody, headers: headers).responseJSON{ response in
            switch response.result {
            case .success(let value):
                print("result \(value)")
                
                    
                
            case .failure(let error):
                print(error)
//                compilition(nil, error.localizedDescription)
            }
        }
    }
}
