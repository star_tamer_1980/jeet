//
//  addCartAdapter.swift
//  Jeet
//
//  Created by Mac on 8/7/21.
//

import Foundation
import Alamofire
import SwiftyJSON

class addCartAdapter: API, ObservableObject{
    
    let url = APIURL.baseUrl + "wp-json/jeet/user/update/cart"
    
    func addProductToCart(token: String?, products: String? = "", completion: @escaping ()->Void) {
        let headers : HTTPHeaders = [
            "Authorization": "Bearer " + token!
        ]
        var parameters : [String:Any] = [:]
        if (products != ""){
            parameters["products"] = products
        }
        
        
        AF.request(url, method: .post, parameters: parameters, encoding: URLEncoding.httpBody, headers: headers).responseJSON{ response in
                    switch response.result {
                    case .success(let value):
                            let json = JSON(value)
                            print(value)
        
                        completion()
        
                    case .failure(let error):
                        print("error \(error)")
                        print(error.errorDescription)
                        completion()
                    }
                }
    
    }
    
    
}

