//
//  catListAdapter.swift
//  Jeet
//
//  Created by Mac on 6/1/21.
//

import SwiftUI

import Alamofire
import SwiftyJSON

class catListObserval: API, ObservableObject{
    
    // Start pagination -----------------------------------

    // get data now
    var isLoading = false
    
    // is found new data or get all data in server?
    var isMore = true
    
    // End pagination -------------------------------------
    
    let url = APIURL.baseUrl + "wp-json/jeet/get/categories"
    let headers : HTTPHeaders = []
    
    
    
    
    @Published var list = [catShort]()
    
    
    func getData(limit: Int? = 12, paged: Int? = 1, search: String? = "", product_id: Int? = 0, taxonomy: String? = "") {
        
        var parameters : [String:Any] = [:]
        if (limit != 0){
            parameters["limit"] = limit
        }
        if (paged != 0){
            parameters["paged"] = paged
        }
        if (search != ""){
            parameters["search"] = search
        }
        if (product_id != 0){
            parameters["product_id"] = product_id
        }
        if (taxonomy != ""){
            parameters["taxonomy"] = taxonomy
        }
        
        if(self.isLoading){
            return
        }
        if(self.isMore == false){
            return
        }
        self.isLoading = true
        
        AF.request(url, method: .post, parameters: parameters, encoding: URLEncoding.httpBody, headers: headers).responseJSON{ response in
                    switch response.result {
                    case .success(let value):
                            let json = JSON(value)
                            guard let dataArr = json["data"].array else{
                                print("error from json")
                                return
                            }
                        self.EndPagination(limitList: json["total"].intValue, countList: (paged! * limit!))
        
                            for data in dataArr{
                                guard let data = data.dictionary else {return}
                                let row = catShort(id: data["id"]?.intValue ?? 0, name: data["name"]?.stringValue ?? "", image: (data["image"]?.stringValue ?? "").fixedArabicURL!, description: data["description"]?.stringValue ?? "", parent: data["parent"]?.intValue ?? 0, total_products: data["total_products"]?.intValue ?? 0)
                                self.list.append(row)
                            }
                        
        
                    case .failure(let error):
                        print("error \(error)")
                        print(error.errorDescription)
        
                    }
                }
    }
    
    private func EndPagination(limitList: Int?, countList: Int?){
        self.isLoading = false
        if(limitList == countList){
            self.isMore = false
        }
    }
}
