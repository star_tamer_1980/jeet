//
//  productDetailsAdapter.swift
//  Jeet
//
//  Created by Mac on 6/4/21.
//

import SwiftUI
import Alamofire
import SwiftyJSON

class productsDetailsObserval: API, ObservableObject{
    
    var url = APIURL.baseUrl + "wp-json/jeet/get/products/"
    let headers : HTTPHeaders = []
    
    @Published var productDetails = productDetailsModel()
    
    func getData(ID: Int?, _ completion: @escaping ()-> Void){
        print("id details is : \(ID)")
        url = url + String(ID ?? 0)
        
        var parameters : [String:Any] = [:]
       
        
        
        AF.request(url, method: .post, parameters: parameters, encoding: URLEncoding.httpBody, headers: headers).responseJSON{ response in
                    switch response.result {
                    case .success(let value):
                            let json = JSON(value)
                        print(value)
                            guard let dataArr = json["data"].array else{
                                print("error from json")
                                completion()
                                return
                            }
        
                            for data in dataArr{
                                guard let data = data.dictionary else {return}
                                let ratingCount = ratingCountModel(first: data["rating_count"]!["1"].intValue , second: data["rating_count"]!["2"].intValue , third: data["rating_count"]!["3"].intValue, fourth: data["rating_count"]!["4"].intValue, fifth: data["rating_count"]!["5"].intValue)
//                                let ratingCount = data["rating_count"] as! ratingCountModel
                                var attr = [attributesModel]()
                                
                                guard let attibutes = data["attributes"]?.array else{
                                    return
                                }
                                for attrb in attibutes{
                                    guard let data = attrb.dictionary else {return}
                                    let a = attributesModel(id: data["id"]?.intValue, taxonomy: data["taxonomy"]?.stringValue, name: data["name"]?.stringValue, values: [data["values"]![0].stringValue])
                                    attr.append(a)
                                }
                                
//                                var shipTitle = ""
//                                var ship = [shipToModel]()
//
//                                guard let shipList = data["ship_to"]?.array else{
//                                    return
//                                }
//                                for ships in shipList{
//                                    guard let data = ships.dictionary else {return}
//                                    let a = shipToModel(id: data["id"]?.intValue ?? 0, city: data["city"]?.stringValue ?? "")
//                                    ship.append(a)
//                                    shipTitle = shipTitle + " , " + data["city"]!.stringValue
//                                }
                                
                                
                                let row = productDetailsModel(id: data["id"]?.intValue ?? 0,
                                                              name: data["name"]?.stringValue ?? "",
                                                              regular_price: data["regular_price"]?.stringValue ?? "",
                                                              price: data["price"]?.stringValue ?? "",
                                                              image: (data["image"]?.stringValue ?? "").fixedArabicURL!,
                                                              featured: data["featured"]?.boolValue ?? false,
                                                              date_created: data["image"]?.stringValue ?? "",
                                                              reviews_allowed: data["reviews_allowed"]?.boolValue ?? false,
                                                              reviews_count: data["reviews_count"]?.intValue ?? 0,
                                                              average_rating: data["average_rating"]?.intValue ?? 0,
                                                              quantity: data["quantity"]?.intValue ?? 0,
                                                              store_name: data["store_name"]?.stringValue ?? "",
//                                                              ship_to_title: shipTitle,
                                                              rating_count: ratingCount,
                                                              attributes: attr)
//                                let row = productShort(id: data["id"]?.intValue ?? 0, name: data["name"]?.stringValue ?? "", regular_price: data["regular_price"]?.stringValue ?? "", price: data["price"]?.stringValue ?? "", image: data["image"]?.stringValue ?? "", in_cart: data["in_cart"]?.boolValue ?? false, in_favorite: data["in_favorite"]?.boolValue ?? false, average_rating: data["average_rating"]?.stringValue ?? "0.0", featured: data["featured"]?.boolValue ?? false)
                                self.productDetails = row
                            }
                        completion()
                        
        
                    case .failure(let error):
                        print("error \(error)")
                        print(error.errorDescription)
                        completion()
                    }
                }
    }
    
    
}
