//
//  profileAdapter.swift
//  Jeet
//
//  Created by Mac on 7/24/21.
//

import SwiftUI
import Alamofire
import SwiftyJSON

class profileObservable: API, ObservableObject{
    let url = APIURL.baseUrl + "wp-json/jeet/user/get/details"
    let headers : HTTPHeaders = []
    
    func details(_ token: String, _ compilition: @escaping(_ userData: userModel?, _ error: String?)->Void){
        var parameter : [String: Any] = [:]
        parameter["oauth_token"] = token
        
        AF.request(url, method: .post, parameters: parameter, encoding: URLEncoding.httpBody, headers: headers).responseJSON{ response in
            switch response.result {
            case .success(let value):
                print("details \(value)")
                let json = JSON(value)
                if json["code"].intValue == 200{
                    var user = userModel()
                    user.isLogin = true
                    user.fName = json["data"]["first_name"].stringValue
                    user.lName = json["data"]["last_name"].stringValue
                    user.username = json["data"]["username"].stringValue
                    user.email = json["data"]["email"].stringValue
                    user.phone = json["data"]["phone_number"].stringValue
                    user.phoneVerify = json["data"]["phone_number_verified"].boolValue
                    user.address?.fName = json["address"]["billing_first_name"].stringValue
                    user.address?.lName = json["address"]["billing_last_name"].stringValue
                    user.address?.title = json["address"]["billing_address_1"].stringValue
                    user.address?.city = json["address"]["billing_city"].stringValue
                    user.address?.governorateId = json["address"]["billing_governorate"].stringValue
                    user.address?.governorateName = json["address"]["billing_governorate_name"].stringValue
                    user.address?.state = json["address"]["billing_state"].stringValue
                    user.address?.postcode = json["address"]["billing_postcode"].stringValue
                    user.address?.country = json["address"]["billing_country"].stringValue
                    user.address?.latitude = json["address"]["billing_latitude"].stringValue
                    user.address?.longitude = json["address"]["billing_longitude"].stringValue
                    user.address?.placeId = json["address"]["billing_place_id"].stringValue
                    compilition(user, "")
                    
                }
                compilition(nil, "لا يوجد بيانات")
                    
                
            case .failure(let error):
                print(error)
                compilition(nil, error.localizedDescription)
            }
        }
    }
}
