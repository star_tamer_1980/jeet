//
//  profileModifireAdapter.swift
//  Jeet
//
//  Created by Mac on 7/25/21.
//

import Foundation

import Alamofire
import SwiftyJSON

class profileModifireObservable: API, ObservableObject{
    let url = APIURL.baseUrl + "wp-json/jeet/user/update/account"
    let headers : HTTPHeaders = []
    
    func sendData(_ userData: userModel?, _ complition: @escaping (_ status: Bool?, _ message: String?)->Void){
        var parameter : [String: Any] = [:]
        parameter["oauth_token"] = userData?.token
        parameter["account_email"] = userData?.email
        parameter["account_phone"] = userData?.phone
        parameter["account_first_name"] = userData?.fName
        parameter["account_last_name"] = userData?.lName
        
        AF.request(url, method: .post, parameters: parameter, encoding: URLEncoding.httpBody, headers: headers).responseJSON{ response in
            switch response.result {
            case .success(let value):
                print(value)
                let json = JSON(value)
                print(json)
                if(json["data"]["success"].boolValue == true){
                    complition(json["data"]["success"].boolValue, json["data"]["message"].stringValue)
                }else{
                    complition(false, json["data"]["errors"][0].stringValue)
                }
                
                    
                
            case .failure(let error):
                print(error)
                complition(false, error.localizedDescription)
            }
        }
    }
}
