//
//  otpObservable.swift
//  Jeet
//
//  Created by Mac on 7/24/21.
//

import SwiftUI
import Alamofire
import SwiftyJSON

class otbObserval : API, ObservableObject{
    let url = APIURL.baseUrl + "wp-json/jeet/user/send-otp"
    let urlverify = APIURL.baseUrl + "wp-json/jeet/user/verify-otp"
    let headers : HTTPHeaders = []
    
    func send(_ phoneNumber: String?, _ token: String?, _ complition: @escaping(_ status: Bool?, _ message: String?) -> Void){
        var parameter: [String: Any] = [:]
        parameter["number"] = phoneNumber
        parameter["oauth_token"] = token
        
        AF.request(url, method: .post, parameters: parameter, encoding: URLEncoding.httpBody, headers: headers).responseJSON{ response in
            switch response.result{
            case .success(let value):
                print(value)
                let json = JSON(value)
                if json["success"].stringValue != ""{
                    complition(true, "")
                }else{
                    complition(false, json["message"].stringValue)
                }
            case .failure(let error):
                print("error \(error.localizedDescription)")
                complition(false, error.localizedDescription)
            }
        }
    }
    
    func verify(_ code: String?, _ token: String?, _ complition: @escaping(_ status: Bool?, _ message: String?) -> Void){
        var parameter: [String: Any] = [:]
        parameter["otp_code"] = code
        parameter["oauth_token"] = token
        
        AF.request(urlverify, method: .post, parameters: parameter, encoding: URLEncoding.httpBody, headers: headers).responseJSON{ response in
            switch response.result{
            case .success(let value):
                print(value)
                let json = JSON(value)
                if json["success"].stringValue != ""{
                    complition(true, json["message"].stringValue)
                }else{
                    complition(false, json["message"].stringValue)
                }
            case .failure(let error):
                print("error \(error.localizedDescription)")
                complition(false, error.localizedDescription)
            }
        }
    }

}
