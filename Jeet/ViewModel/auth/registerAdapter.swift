//
//  registerAdapter.swift
//  Jeet
//
//  Created by Mac on 6/19/21.
//

import Foundation
import Alamofire
import SwiftyJSON

class registerObservable: API, ObservableObject{
    let url = APIURL.baseUrl + "wp-json/jeet/user/register"
    let headers : HTTPHeaders = []
    
    func sendData(_ userData: userModel?, _ complition: @escaping (_ userData: userModel?, _ status: Bool?, _ message: String?)->Void){
        var parameter : [String: Any] = [:]
        parameter["email"] = userData?.email
        parameter["username"] = userData?.username
        parameter["phone"] = userData?.phone
        parameter["password"] = userData?.password
        parameter["first_name"] = userData?.fName
        parameter["last_name"] = userData?.lName
        parameter["role"] = "customer"
        
        AF.request(url, method: .post, parameters: parameter, encoding: URLEncoding.httpBody, headers: headers).responseJSON{ response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                if json["code"].intValue == 200{
                    var user = userModel()
                    user.isLogin = true
                    user.token = json["token"].stringValue
                    user.niceName = json["user_nicename"].stringValue
                    user.displayName = json["user_display_name"].stringValue
                    user.email = json["user_email"].stringValue
                    user.phone = json["user_phone"].stringValue
                    
                    
                    complition(user, true, json["message"].stringValue)
                }else{
                    complition(nil, false, json["message"].stringValue)
                }
            case .failure(let error):
                print(error)
                complition(nil, false, error.localizedDescription)
            }
        }
    }
}
