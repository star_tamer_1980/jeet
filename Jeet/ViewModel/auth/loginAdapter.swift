//
//  loginAdapter.swift
//  Jeet
//
//  Created by Mac on 6/19/21.
//

import Foundation
import Alamofire
import SwiftyJSON

class loginObserval : API, ObservableObject{
    let url = APIURL.baseUrl + "wp-json/jwt-auth/v1/token"
    let headers : HTTPHeaders = []
    
    func login(_ userData: userModel?, _ complition: @escaping(_ userData: userModel?, _ status: Bool?, _ message: String?) -> Void){
        var parameter: [String: Any] = [:]
        parameter["username"] = userData?.email
        parameter["password"] = userData?.password
        
        AF.request(url, method: .post, parameters: parameter, encoding: URLEncoding.httpBody, headers: headers).responseJSON{ response in
            switch response.result{
            case .success(let value):
                print("login \(value)")
                let json = JSON(value)
                if json["token"].stringValue != ""{
                    var user = userModel()
                    user.isLogin = true
                    user.token = json["token"].stringValue
                    user.niceName = json["user_nicename"].stringValue
                    user.displayName = json["user_display_name"].stringValue
                    user.email = json["user_email"].stringValue
                    user.phone = json["user_phone"].stringValue
                    complition(user, true, "successfully login")
                }else{
                    complition(nil, false, json["message"].stringValue)
                }
            case .failure(let error):
                print("error \(error.localizedDescription)")
                complition(nil, false, error.localizedDescription)
            }
        }
    }
}
