//
//  productCartCoreDataAdapter.swift
//  Jeet
//
//  Created by Mac on 6/13/21.
//

import SwiftUI
import CoreData

class productCartCoreDataObservable: ObservableObject {
    
    let coreDM = CoreDataManager()
    @Published var list = [ProductListCoreModel]()
    
    func getAllData(_ complition: @escaping ()-> Void) {
        self.list = coreDM.getAllProductsCart()
        complition()
    }
    func deleteProduct(product: ProductListCoreModel) {
        coreDM.deleteProduct(product: product)
    }
    func calculateProducts()->String{
        var pro : String = ""
        var i = 0
        self.list.forEach { (product) in
            if(i == 0){
                pro = pro + "{\"product_id\":"+String(product.id)+",\"quantity\":"+String(product.qty)+"}"
            }else{
                pro = pro + ",{\"product_id\":"+String(product.id)+",\"quantity\":"+String(product.qty)+"}"
            }
            i = i + 1
        }
        return "[" + pro + "]"
    }
}
