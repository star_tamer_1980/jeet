//
//  addressLocalAdapter.swift
//  Jeet
//
//  Created by Mac on 6/21/21.
//

import Foundation
import MapKit
class addressLocalObserval : ObservableObject{
    @Published var annotation = MKPointAnnotation()
    func addLocation(_ location: MKPointAnnotation?){
        self.annotation = location!
    }
    func getLocation(_ completion: @escaping (_ anno: MKPointAnnotation?)-> Void){
        completion(annotation)
        
    }
}
