//
//  addressObserver.swift
//  Jeet
//
//  Created by Mac on 7/3/21.
//

import Foundation
import Alamofire
import SwiftyJSON

class addressObservable: API, ObservableObject{
    let url = APIURL.baseUrl + "wp-json/jeet/geo/address"
    let headers : HTTPHeaders = []
    
    func getAddress(_ latitude: String?, _ longitude: String?, _ complition: @escaping (_ address: addressModel?, _ status: Bool?, _ message: String?)->Void){
        var parameter : [String: Any] = [:]
        parameter["lat"] = latitude
        parameter["lng"] = longitude
        
        AF.request(url, method: .post, parameters: parameter, encoding: URLEncoding.httpBody, headers: headers).responseJSON{ response in
            switch response.result {
            case .success(let value):
                print(value)
                let json = JSON(value)
                if json["success"].boolValue == true{
                    var location = addressModel()
                    location.postal_code = json["postal_code"].stringValue
                    location.country = json["country"].stringValue
                    location.country_code = json["country_code"].stringValue
                    location.city = json["city"].stringValue
                    location.state = json["state"].stringValue
                    location.plus_code = json["plus_code"].stringValue
                    location.place_id = json["place_id"].stringValue
                    location.address = json["address"].stringValue
                    location.location?.latitude = json["location"]["lat"].doubleValue
                    location.location?.longitude = json["location"]["lng"].doubleValue
                    complition(location, true, "location get successfully")
                }else{
                    complition(nil, false, json["message"].stringValue)
                }
            case .failure(let error):
                print(error)
                complition(nil, false, error.localizedDescription)
            }
        }
    }
}

