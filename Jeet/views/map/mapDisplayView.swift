//
//  mapDisplayView.swift
//  Jeet
//
//  Created by Mac on 6/20/21.
//

import SwiftUI
import MapKit

struct mapDisplayView: View {
    @Binding var showMap: Bool
    @Binding var address: addressModel
    @Binding var addressText: String
    @Binding var latitude: String
    @Binding var Longitude: String
    @State var showAlert: Bool = false
    @State var showAlertChooseLocation: Bool = false
    @State var showProgress = false
    
    @ObservedObject var addressObserv = addressObservable()
    var body: some View {
        
        VStack{
            HStack{
                Spacer()
                Text("اختيار موقعك علي الخريطه")
                    .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 15)
                    .lineLimit(1)
                Spacer()
                Button(action: {
                    self.showMap.toggle()
                }, label: {
                    Image(systemName: "arrow.backward")
                        .resizable()
                        .frame(width: 20, height: 20)
                        .foregroundColor(colorPallet.gray)
                        .environment(\.layoutDirection, .leftToRight)
                })
                .padding(.trailing, 10)
            }
            .padding()
            .background(colorPallet.bgCard)
            .shadow(radius: 1)
            Spacer()
            VStack{
                mapView(latitude: $latitude, longitude: $Longitude)
                Spacer()
                Button(action: {
                    Print("latitude \(latitude)")
                    Print("longitude \(Longitude)")
                    if(latitude == nil || Longitude == nil){
                        showAlertChooseLocation.toggle()
                    }
                    showProgress = true
                    addressObserv.getAddress(latitude, Longitude) { (address, status, message) in
                        showProgress = false
                        if(status == true){
                            self.address = address!
                            self.addressText = (address?.address)!
                        }
                        self.showMap.toggle()
                    }
                }, label: {
                    Text("حفظ العنوان")
                        .padding()
                        .padding(.leading, 24)
                        .foregroundColor(colorPallet.white)
                        .frame(minWidth: 0, maxWidth: .infinity)
                        .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 15)
                }).background(colorPallet.move)
                .cornerRadius(10)
                .frame(height: 50)
                .padding()
            }
            .overlay(
                showProgress ? progress() : nil
            )
            .alert(isPresented: $showAlert) { () -> Alert in
                Alert(title: Text("تحذير"), message: Text("يوجد خطأ في الحصول علي العنوان"), dismissButton: .default(Text("موافق"), action: {
                    self.showMap.toggle()
                }))
            }
            .alert(isPresented: $showAlertChooseLocation) { () -> Alert in
                Alert(title: Text("تحذير"), message: Text("يجب اختيار موقع علي الخريطه"), dismissButton: .default(Text("موافق"), action: {
                    self.showMap.toggle()
                }))
            }
            
        }
    }
}


