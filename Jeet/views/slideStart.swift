//
//  slideStart.swift
//  Jeet
//
//  Created by Mac on 5/19/21.
//

import SwiftUI


struct slideStart: View{
    @ObservedObject var settings = UserSettings()
    @State var currentIndex: Int = 1
    var data : [CardCarusualModel] = [
        CardCarusualModel(id: 1, image: "carsual1", title: "تسوق أفضل المنتجات الالكترونية1", description: " واحد من أكبر وأشهر مواقع تجارة التجزئة عبر الإنترنت، وهو المنافس"),
        CardCarusualModel(id: 2, image: "carsual2", title: "تسوق أفضل المنتجات الالكترونية2", description: " واحد من أكبر وأشهر مواقع تجارة التجزئة عبر الإنترنت، وهو المنافس"),
        CardCarusualModel(id: 3, image: "carsual3", title: "تسوق أفضل المنتجات الالكترونية3", description: " واحد من أكبر وأشهر مواقع تجارة التجزئة عبر الإنترنت، وهو المنافس")
    ]
    var body: some View{
            
        if !settings.showOnStart{
            HomeView()
        }else{
            VStack(spacing: 100) {
                VStack{
                    TabView(selection: $currentIndex){
                        ForEach(data) {row in
                            VStack(alignment: .center, spacing: 0) {
                                Image(row.image)
                                    .resizable()
                                    .frame(width: 249, height: 249)
                                Text(row.title)
                                    .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .regular, size: 20)
                                    .padding(.vertical, 13)
                                    .multilineTextAlignment(.center)
                                Text(row.description)
                                    .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .regular, size: 15)
                                    .multilineTextAlignment(.center)
                                    .padding(.top, 13)
                                    
                            }
                            .padding()
                        }
                    }
                    .tabViewStyle(PageTabViewStyle(indexDisplayMode: .never))
                    
                        CustomTapIndecator(count: 3, current: $currentIndex)
                    
                }
                
                VStack(spacing: 10){
                    Button(action: {
                        self.settings.showOnStart.toggle()
                    }, label: {
                        Text("تسوق الآن")
                            .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .bold, size: 20)
                            .foregroundColor(Color.init("whiteColor"))
                            .frame(maxWidth: .infinity, alignment: .center)
                            .padding()
                    })
                    .background(
                        RoundedRectangle(cornerRadius: 10)
                            .fill(Color.init("BGButtonOK"))
                            .overlay(
                            RoundedRectangle(cornerRadius: 10)
                                .stroke(Color.init("whiteColor"), lineWidth: 1)
                            )
                    )
                }
            }
            .padding()
            
            Spacer()
        }
        
    }
}


struct CardCarusualModel: Identifiable {
    var id: Int
    var image: String
    var title: String
    var description: String
}



struct CustomTapIndecator: View {
    var count: Int
    @Binding var current: Int
    var body: some View{
        HStack{
            ForEach(0..<count, id: \.self){index in
                ZStack{
                    if(current - 1) == index{
                        Circle()
                            .fill(Color.init("indecatorActive"))
                    }else{
                        Circle()
                            .fill(Color.init("indecatorDisactive"))
                            .overlay(Circle().stroke(Color.init("indecatorActive"), lineWidth: 1.5))
                    }
                }.frame(width: 10, height: 10)
            }
        }
    }
}
