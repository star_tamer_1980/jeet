//
//  HomeData.swift
//  Jeet
//
//  Created by Mac on 5/25/21.
//


import SwiftUI
import Alamofire
import SwiftyJSON
import Kingfisher


struct HomeData: View {
    @State var openSearch = false
    @State var openProductLatest = false
    @State var openProductLatestDetails = false
    @State var openProductTopRated = false
    @State var openProductTopRatedDetails = false
    @State var productDetailsId = 0
    @State var openCat = false
    @State var openCatList = false
    @State var catDetailsId = 0
    @State var catDetailsTitle = ""
    @State var openBrand = false
    @State var openBrandList = false
    @State var brandDetailsId = 0
    @State var brandDetailsTitle = ""
    @ObservedObject var productList = productsShortListObserval()
    @ObservedObject var productRatingList = productsRatingListObserval()
    @ObservedObject var catList = catListObserval()
    @ObservedObject var brandListData = catListObserval()
    
    init(){
        print("start list")
        catList.getData(limit: 6, taxonomy: "product_cat")
        productList.getList(limit: 2){

        }
        productRatingList.getList(limit: 2)
        brandListData.getData(limit: 6, taxonomy: "brand")
    }
    var body: some View {
        ScrollView {
            
            Group{
                HStack{
                    Text("وصل حديثاً")
                        .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .black, size: 20)
                    Spacer()
                    Button(action: {
                        self.openProductLatest.toggle()
                    }, label: {
                        Text("كل ما وصل حديثاً")
                            .chooseFont(fontManager: .BalooBhaijaan, style: .gray, weight: .medium, size: 14)
                    })
                    .fullScreenCover(isPresented: self.$openProductLatest, content: {
                        latestProducts()
                    })
                }

                LazyVGrid(columns: [
                    GridItem(.flexible(minimum: 150, maximum: 500)),
                    GridItem(.flexible(minimum: 150, maximum: 500))
                ]) {
                    Print("productList = \(productList.list.count)")
                    ForEach(productList.list, id: \.self){ product in
                        Button(action: {
                            self.openProductLatestDetails.toggle()
                            self.productDetailsId = product.id ?? 0
                        }, label: {
                            productCard(product: product)
                        }).fullScreenCover(isPresented: self.$openProductLatestDetails, content: {
                            productDetails(id: self.$productDetailsId)
                        })
                    }


                }.padding(.horizontal, 12)

                Divider()

                HStack{
                    Text("الأعلي تقييماً")
                        .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .black, size: 20)
                    Spacer()
                    Button(action: {
                        self.openProductTopRated.toggle()
                    }, label: {
                        Text("جميع الاعلي تقييماً")
                            .chooseFont(fontManager: .BalooBhaijaan, style: .gray, weight: .medium, size: 14)
                    })
                    .fullScreenCover(isPresented: self.$openProductTopRated, content: {
                        toprated()
                    })
                }
                LazyVGrid(columns: [
                    GridItem(.flexible(minimum: 150, maximum: 500)),
                    GridItem(.flexible(minimum: 150, maximum: 500))
                ]) {
                    Print("productRatingList = \(productRatingList.list.count)")
                    ForEach(productRatingList.list, id: \.self){ product in
                        Button(action: {
                            self.openProductTopRatedDetails.toggle()
                            self.productDetailsId = product.id ?? 0
                        }, label: {
                            productCard(product: product)
                        }).fullScreenCover(isPresented: self.$openProductTopRatedDetails, content: {
                            productDetails(id: self.$productDetailsId)
                        })
                    }


                }.padding(.horizontal, 12)
            }


        }
        .padding()
    }
}


struct HomeData_Previews: PreviewProvider {
    static var previews: some View {
        HomeData()
    }
}


