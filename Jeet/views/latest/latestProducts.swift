//
//  latestProducts.swift
//  Jeet
//
//  Created by Mac on 6/1/21.
//

import SwiftUI

struct latestProducts: View {
    
    @State var titleView = "وصل حديثاً"
    @State var searchWord = ""
    @State var isSearching = false
    
    @State var navigated = false
    @State var productDetailsId = 0
    @State var showProgress = true
    
    @State var showActionSheet = false
    
    
    @State var limit = 10
    @State var paged = 1
    @State var order = "desc"
    @State var orderBy = "date"
    @State var category_id = 0
    @State var taxonomy = "product_cat"
//    @ObservedObject var productList = productsListObserval(limit: 10)
    @ObservedObject var productList = productsShortListObserval()
    @Environment(\.presentationMode) var presentationMode
    
    var body: some View {
            VStack{
                HStack{
                    Image(systemName: "slider.vertical.3")
                        .resizable()
                        .frame(width: 20, height: 20)
                        .foregroundColor(colorPallet.gray)
                        .environment(\.layoutDirection, .leftToRight)
                        .onTapGesture {
                            self.showActionSheet = true
                        }
                        .actionSheet(isPresented: $showActionSheet){
                            ActionSheet(title: Text("عرض بحسب"), message: Text("ترتيب المنتجات بحسب"), buttons: [
                                            .default(Text("الأعلي تقييماً")){
                                                
                                            },
                                            .default(Text("الأحدث ")){
                                                DispatchQueue.main.async {
                                                    self.order = "desc"
                                                    productList.list.removeAll()
                                                    Print(self.order)
                                                    productList.getList(limit: self.limit, paged: self.paged, order: self.order){
                                                        
                                                    }
                                                }
                                            },
                                            .default(Text("الأقدم")){
                                                DispatchQueue.main.async {
                                                    self.order = "asc"
                                                    productList.list.removeAll()
                                                    Print(self.order)
                                                    productList.getList(limit: self.limit, paged: self.paged, order: self.order){
                                                        
                                                    }
                                                }
                                            },
                                            .default(Text("الأقل سعراً")){
                                                
                                            },
                                            .default(Text("الأعلي سعراً")){
                                                
                                            },
                                            .cancel()
                            ])
                        }
                    
                    Spacer()
                    Text(self.titleView)
                    Spacer()
                    Button(action: {
                        presentationMode.wrappedValue.dismiss()
                    }, label: {
                        Image(systemName: "arrow.backward")
                            .resizable()
                            .frame(width: 20, height: 20)
                            .foregroundColor(colorPallet.gray)
                            .environment(\.layoutDirection, .leftToRight)
                    })
                    .padding(.trailing, 10)
                }
                .padding()
                .background(colorPallet.bgCard)
                .shadow(radius: 1)
                Spacer()
                
                ScrollView{
                    LazyVGrid(columns: [
                        GridItem(.flexible(minimum: 150, maximum: 500)),
                        GridItem(.flexible(minimum: 150, maximum: 500))
                    ]) {
                        ForEach(productList.list, id: \.self){ product in
                            Button(action: {
                                self.navigated.toggle()
                                self.productDetailsId = product.id ?? 0
                            }, label: {
                                if (productList.list[productList.list.count - 1].id == product.id){
                                    productCard(product: productList.list[getIndex(product: product)], isLast: true, isLoadMore: productList.isMore)
                                        .onAppear(){
                                            self.paged = self.paged + 1
                                            DispatchQueue.main.async {
                                                productList.getList(limit: self.limit, paged: self.paged, order: self.order, orderby: self.orderBy, category_id: self.category_id, taxonomy: self.taxonomy, searchWord: self.searchWord){
                                                }
                                            }
                                        }
                                }else{
                                    productCard(product: productList.list[getIndex(product: product)])
                                }
                                
                            }).fullScreenCover(isPresented: self.$navigated, content: {
                                productDetails(id: self.$productDetailsId)
                            })

                        }


                    }
                    .padding()
                    .padding(.horizontal, 12)
                }.zIndex(1.0)
                .overlay(
                    showProgress ? progress() : nil
                )
                .onAppear(){
                    DispatchQueue.main.async {
                            productList.getList(limit: self.limit, paged: self.paged, order: self.order, orderby: self.orderBy, category_id: self.category_id, taxonomy: self.taxonomy, searchWord: self.searchWord){
                            showProgress = false
                        }
                    }
                }
            }.background(colorPallet.white)
            
        
    }
    
    func getIndex(product: productShort)->Int{
        return productList.list.firstIndex { (product1) -> Bool in
            return product.id == product1.id
        } ?? 0
    }
}


struct latestProducts_Previews: PreviewProvider {
    static var previews: some View {
        latestProducts()
        
    }
}
