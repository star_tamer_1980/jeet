//
//  searchData.swift
//  Jeet
//
//  Created by Mac on 5/25/21.
//

import SwiftUI

struct searchData: View {
    
    // search Settings
    @Binding var searchWord : String
    @Binding var isSearching: Bool
    @State var navigated = false
    @State var productDetailsId = 0
    @State var showProgress = true
    
    
    @State var limit = 10
    @State var paged = 1
    
    
    @ObservedObject var productList = productsShortListObserval()
    var body: some View {
        ScrollView{
            LazyVGrid(columns: [
                GridItem(.flexible(minimum: 150, maximum: 500)),
                GridItem(.flexible(minimum: 150, maximum: 500))
            ]) {
                ForEach(productList.list, id: \.self){ product in
                    Button(action: {
                        self.navigated.toggle()
                        self.productDetailsId = product.id ?? 0
                    }, label: {
                        if (productList.list[productList.list.count - 1].id == product.id){
                            productCard(product: product, isLast: true, isLoadMore: productList.isMore)
                                .onAppear(){
                                    self.paged = self.paged + 1
                                    DispatchQueue.main.async {
                                        productList.getList(limit: self.limit, paged: self.paged, searchWord: self.searchWord){
                                        }
                                    }
                                }
                        }else{
                            productCard(product: product)
                        }
                        
                    }).fullScreenCover(isPresented: self.$navigated, content: {
                        productDetails(id: self.$productDetailsId)
                    })

                }


            }.padding(.horizontal, 12)
        }.padding()
        .overlay(
            showProgress ? progress() : nil
        )
        .onAppear(){
            DispatchQueue.main.async {
                productList.list.removeAll()
                    productList.getList(limit: self.limit, paged: self.paged, searchWord: self.searchWord){
                        self.isSearching = true
                        showProgress = false
                }
            }
        }
        
        
    }
}
