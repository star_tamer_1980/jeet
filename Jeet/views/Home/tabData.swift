//
//  tabData.swift
//  Jeet
//
//  Created by Mac on 5/24/21.
//

import SwiftUI

struct CardTapModel: Identifiable {
    var id: Int
    var image: String
    var title: String
}

struct CustomTapIndecatorHome: View {
    var count: Int
    @Binding var current: Int
    var row: [CardTapModel]
    var body: some View{
        HStack(alignment: .center, spacing: 5){
            ForEach(0..<count, id: \.self){index in
                ZStack(alignment: Alignment(horizontal: .center, vertical: .bottom)){
                    Button(action: {
                        print("current index = \(index + 1)")
                        current = index + 1
                    }, label: {
                        if(current - 1) == index{
                            VStack{
                                Image(systemName: row[index].image)
                                    .foregroundColor(colorPallet.move)
                                Text(row[index].title)
                                    .chooseFont(fontManager: .BalooBhaijaan, style: .move, weight: .regular, size: 10)
                                    .padding(2)
                            }
                        }else{
                            VStack{
                                Image(systemName: row[index].image)
                                    .foregroundColor(.gray)
                                Text(row[index].title)
                                    .chooseFont(fontManager: .BalooBhaijaan, style: .gray, weight: .regular, size: 10)
                                    .padding(2)
                            }
                        }
                    })
                    
                    
                }.frame(width: UIScreen.main.bounds.size.width / 5)
                
            }
        }
    }
}


