//
//  Menu.swift
//  Jeet
//
//  Created by Mac on 5/24/21.
//

import SwiftUI

struct Menu : View {
    @Binding var dark : Bool
    @Binding var showMenu : Bool
    
    @State var showLogin: Bool
    @State var showRegister: Bool
    @State var showProfileModify = false
    @State var showProfilePhoneNumber = false
    @State var showFavourite = false
    @State var showAddress = false
    @State var showStartOTP = false
    @State var showCategory = false
    @State var showBrand = false
    @State var showAbout = false
    @State var showContact = false
    @State var alertResult = false
    @State var alertLogoutIsPresented = false
    
    @ObservedObject var settings = UserSettings()
    var body: some View{
        Print("token \(settings.token)")
        VStack{
            HStack{
                Button(action: {
                    withAnimation(.default){
                        self.showMenu.toggle()
                    }
                }, label: {
                    Image(systemName: "arrow.backward")
                })
                Spacer()
                Button(action: {}, label: {
                    Image(systemName: "square.and.pencil")
                })
            }
            .padding(.top)
            .padding(.bottom, 25)
            
            Image(systemName: "person.circle.fill")
                .resizable()
                .frame(width: 80, height: 80)
                .clipShape(Circle())
            
            VStack(alignment: .leading, spacing: 12, content: {
                Text(settings.displayName)
                    .chooseFont(fontManager: .BalooBhaijaan, style: .white, weight: .medium, size: 15)
            })
            .padding(.top, 15)
            
            
            Divider()
                .background(.gray)
                .padding(.top, 5)
            
            
            HStack(alignment: .center, spacing: 5, content: {
                Image(systemName: "moon.fill")
                    .chooseFont(fontManager: .BalooBhaijaan, style: .white, weight: .medium, size: 15)
                Text("النظام المظلم")
                    .chooseFont(fontManager: .BalooBhaijaan, style: .white, weight: .medium, size: 15)
                Spacer()
                Toggle(isOn: $dark, label: {
                    
                }).onTapGesture {
                    UIApplication.shared.windows.first?.rootViewController?.view.overrideUserInterfaceStyle = self.dark ? .dark : .light
                }
            })
            .padding(.top, 15)
            
            
            Divider()
                .background(.gray)
                .padding(.top, 5)
            
            
            Group{
                if(settings.isLogin == true){
                    Button(action: {
                        self.showProfileModify.toggle()
                        self.showMenu.toggle()
                    }, label: {
                        HStack(spacing: 5){
                            HStack(spacing: 7){
                                Image(systemName: "person.circle.fill")
                                    .chooseFont(fontManager: .BalooBhaijaan, style: .white, weight: .medium, size: 15)
                                Text("بيانات الحساب")
                                    .chooseFont(fontManager: .BalooBhaijaan, style: .white, weight: .medium, size: 15)
                                Spacer()
                            }
                        }
                    }).fullScreenCover(isPresented: self.$showProfileModify, content: {
                        modifyProfile()
                    })
                    
                    Button(action: {
                        self.showProfilePhoneNumber.toggle()
                        self.showMenu.toggle()
                    }, label: {
                        
                        HStack(spacing: 7){
                            Image(systemName: "iphone.radiowaves.left.and.right")
                                .chooseFont(fontManager: .BalooBhaijaan, style: .white, weight: .medium, size: 15)
                            Text("تغيير رقم الهاتف")
                                .chooseFont(fontManager: .BalooBhaijaan, style: .white, weight: .medium, size: 15)
                            Spacer()
                        }
                    }).fullScreenCover(isPresented: self.$showProfilePhoneNumber, content: {
                        modifyPhoneNumber()
                    })
                    if(settings.phoneVerify == false){
                        Button(action: {
                            self.showStartOTP.toggle()
                            self.showMenu.toggle()
                        }, label: {
                            
                            HStack(spacing: 7){
                                Image(systemName: "iphone.homebutton")
                                    .chooseFont(fontManager: .BalooBhaijaan, style: .white, weight: .medium, size: 15)
                                Text("تفعيل رقم الهاتف")
                                    .chooseFont(fontManager: .BalooBhaijaan, style: .white, weight: .medium, size: 15)
                                Spacer()
                            }
                        }).fullScreenCover(isPresented: self.$showStartOTP, content: {
                            authPhoneNumber()
                        })
                    }
                    
                    
                    Divider()
                        .background(.gray)
                        .padding(.top, 5)
                    
                    
                    Button(action: {
                        self.showFavourite.toggle()
                        self.showMenu.toggle()
                    }, label: {
                        HStack(spacing: 7){
                            Image(systemName: "cart.badge.plus")
                                .chooseFont(fontManager: .BalooBhaijaan, style: .white, weight: .medium, size: 15)
                            Text("المفضله")
                                .chooseFont(fontManager: .BalooBhaijaan, style: .white, weight: .medium, size: 15)
                            Spacer()
                        }
                    }).fullScreenCover(isPresented: self.$showFavourite, content: {
                        favouriteList()
                    })
                    Button(action: {
//                        self.showLogin.toggle()
//                        self.showMenu.toggle()
                    }, label: {
                        HStack(spacing: 7){
                            Image(systemName: "cart.badge.plus")
                                .chooseFont(fontManager: .BalooBhaijaan, style: .white, weight: .medium, size: 15)
                            Text("الطلبات")
                                .chooseFont(fontManager: .BalooBhaijaan, style: .white, weight: .medium, size: 15)
                            Spacer()
                        }
                    }).fullScreenCover(isPresented: self.$showLogin, content: {
//                        loginListAuth()
                    })
                    
                    Button(action: {
                        self.showAddress.toggle()
                        self.showMenu.toggle()
                    }, label: {
                        HStack(spacing: 7){
                            Image(systemName: "house")
                                .chooseFont(fontManager: .BalooBhaijaan, style: .white, weight: .medium, size: 15)
                            Text("عنوان الشحن")
                                .chooseFont(fontManager: .BalooBhaijaan, style: .white, weight: .medium, size: 15)
                            Spacer()
                        }
                    }).fullScreenCover(isPresented: self.$showAddress, content: {
                        addressData()
                    })
                    
                    Divider()
                        .background(.gray)
                        .padding(.top, 5)
                    
                    
                    
                    
                    Button(action: {
                        self.alertLogoutIsPresented = true
                        settings.isLogin = false
                        settings.displayName = ""
                        settings.nicename = ""
                        settings.token = ""
                        
                    }, label: {
                            HStack(spacing: 7){
                                Image(systemName: "lock.rectangle.on.rectangle.fill")
                                    .chooseFont(fontManager: .BalooBhaijaan, style: .white, weight: .medium, size: 15)
                                Text("خروج")
                                    .chooseFont(fontManager: .BalooBhaijaan, style: .white, weight: .medium, size: 15)
                                Spacer()
                            }
                    })
                    
                }else{
                    Button(action: {
                        self.showLogin.toggle()
                        self.showMenu.toggle()
                    }, label: {
                        HStack(spacing: 5){
                            Image(systemName: "lock.rectangle.on.rectangle.fill")
                                .chooseFont(fontManager: .BalooBhaijaan, style: .white, weight: .medium, size: 15)
                            Text("دخول عضو")
                                .chooseFont(fontManager: .BalooBhaijaan, style: .white, weight: .medium, size: 15)
                                
                            Spacer()
                        }
                    }).fullScreenCover(isPresented: self.$showLogin, content: {
                        loginListAuth()
                    })
                    Button(action: {
                        self.showRegister.toggle()
                        self.showMenu.toggle()
                    }, label: {
                        HStack(spacing: 5){
                            Image(systemName: "lock.open.fill")
                                .chooseFont(fontManager: .BalooBhaijaan, style: .white, weight: .medium, size: 15)
                            Text("تسجيل عضو")
                                .chooseFont(fontManager: .BalooBhaijaan, style: .white, weight: .medium, size: 15)
                                
                            Spacer()
                        }
                    }).fullScreenCover(isPresented: self.$showRegister, content: {
                        viewRegister()
                    })
                }
                
                Divider()
                    .background(.gray)
                    .padding(.top, 5)
                    .alert(isPresented: $alertLogoutIsPresented, content: {
                        Alert(title: Text("رساله"), message: Text("تم الخروج بنجاح"), dismissButton: .default(Text("OK"), action: {
                            self.showMenu.toggle()
                        }))
                    })
                
                
                
                
                
                Button(action: {
                    self.showCategory.toggle()
                    self.showMenu.toggle()
                }, label: {
                    HStack(spacing: 5){
                        Image(systemName: "doc.text")
                            .chooseFont(fontManager: .BalooBhaijaan, style: .white, weight: .medium, size: 15)
                        Text("التصنيفات")
                            .chooseFont(fontManager: .BalooBhaijaan, style: .white, weight: .medium, size: 15)
                            
                        Spacer()
                    }
                }).fullScreenCover(isPresented: self.$showCategory, content: {
                    categoryList(taxonomy: "product_cat")
                })
                
                
                Button(action: {
                    self.showBrand.toggle()
                    self.showMenu.toggle()
                }, label: {
                    HStack(spacing: 5){
                        Image(systemName: "doc.text")
                            .chooseFont(fontManager: .BalooBhaijaan, style: .white, weight: .medium, size: 15)
                        Text("البراندات")
                            .chooseFont(fontManager: .BalooBhaijaan, style: .white, weight: .medium, size: 15)
                            
                        Spacer()
                    }
                }).fullScreenCover(isPresented: self.$showBrand, content: {
                    categoryList(taxonomy: "brand")
                })
                
                
                
                Divider()
                    .background(.gray)
                    .padding(.top, 5)
                
                
                
                Button(action: {
                    self.showAbout.toggle()
                    self.showMenu.toggle()
                }, label: {
                    HStack(spacing: 5){
                        Image(systemName: "doc.text")
                            .chooseFont(fontManager: .BalooBhaijaan, style: .white, weight: .medium, size: 15)
                        Text("عن التطبيق")
                            .chooseFont(fontManager: .BalooBhaijaan, style: .white, weight: .medium, size: 15)
                            
                        Spacer()
                    }
                }).fullScreenCover(isPresented: self.$showAbout, content: {
                    about()
                })
                Button(action: {}, label: {
                    HStack(spacing: 5){
                        Image(systemName: "bell.fill")
                            .chooseFont(fontManager: .BalooBhaijaan, style: .white, weight: .medium, size: 15)
                        Text("الاشعارات")
                            .chooseFont(fontManager: .BalooBhaijaan, style: .white, weight: .medium, size: 15)
                            
                        Spacer()
                    }
                })
                Button(action: {
                    self.showContact.toggle()
                    self.showMenu.toggle()
                }, label: {
                    HStack(spacing: 5){
                        Image(systemName: "text.bubble")
                            .chooseFont(fontManager: .BalooBhaijaan, style: .white, weight: .medium, size: 15)
                        Text("اتصل بنا")
                            .chooseFont(fontManager: .BalooBhaijaan, style: .white, weight: .medium, size: 15)
                            
                        Spacer()
                    }
                }).fullScreenCover(isPresented: self.$showContact, content: {
                    Contact()
                })
            }
            .padding(.top, 15)
            Spacer()
        }
        .foregroundColor(.primary)
        .padding(.horizontal, 20)
        .frame(width: UIScreen.main.bounds.width / 1.5)
        .background((colorPallet.move).edgesIgnoringSafeArea(.all))
        .overlay(Rectangle().stroke(Color.primary.opacity(0.2), lineWidth: 2).shadow(radius: 3).edgesIgnoringSafeArea(.all))
    }
}
