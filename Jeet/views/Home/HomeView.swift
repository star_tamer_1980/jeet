//
//  HomeView.swift
//  Jeet
//
//  Created by Mac on 5/19/21.
//

import SwiftUI


struct HomeView: View{
    // menu settings
    @State var dark = false
    @State var showMenu = false
    
    // menu Auth
    @State var showRegister = false
    @State var showLogin = false
    
    // search Settings
    @State var searchWord = ""
    @State var isSearching = false
    
    // tab index settings
    @ObservedObject var settings = UserSettings()
    @State var currentIndex: Int = 1
    var data : [CardTapModel] = [
        CardTapModel(id: 1, image: "house.fill", title: "الرئيسيه"),
        CardTapModel(id: 2, image: "magnifyingglass", title: "بحث"),
        CardTapModel(id: 3, image: "cart.fill", title: "السله"),
        CardTapModel(id: 4, image: "person.fill", title: "البروفايل")
    ]
    var body: some View{
        if self.showLogin == true{
            loginListAuth()
        }
        ZStack(alignment: Alignment(horizontal: .leading, vertical: .top), content: {
            Image("Backgrounds - Navigation Bar - Light")
                .resizable()
                .frame(width: UIScreen.main.bounds.width, height: 150).edgesIgnoringSafeArea(.all)
            GeometryReader{_ in
                VStack{
                    ZStack{
                        
                        HStack{
                            Button(action: {
                                withAnimation(.default){
                                    self.showMenu.toggle()
                                }
                            }, label: {
                                Image(systemName: "text.justifyright")
                                    .resizable()
                                    .frame(width: 15, height: 15)
                                    .foregroundColor(colorPallet.white)
                            })
                            Image("logo")
                                .resizable()
                                .frame(width: 58, height: 46)
                                .padding(.leading, 10)
                            Spacer()
                            Button(action: {
                                
                            }, label: {
                                Image(systemName: "bell.fill")
                                    .resizable()
                                    .frame(width: 15, height: 15)
                                    .foregroundColor(colorPallet.white)
                            })
                            .padding(.trailing, 10)
                        }
                        .padding(.top, -15)
                        .padding(.leading, 1)
                    }.padding()
                    .foregroundColor(.primary)
                    HStack{
                        TextField("هل تبحث عن منتج معين", text: $searchWord)
                            .padding(.leading, 24)
                            .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 12)
                    }
                    .padding(.vertical, 10)
                    .padding(.horizontal, 20)
                    .padding(.leading, -15)
                    .background(colorPallet.white)
                    .cornerRadius(6)
                    .padding(.horizontal)
                    .onTapGesture {
                        isSearching = true
                    }
                    .overlay(
                        HStack{
                            
                            Button(action:{
                                self.isSearching = true
                                
                                self.currentIndex = 2
                            }, label: {
                                Image(systemName: "magnifyingglass")
                            })
                            Spacer()
                            if isSearching{
                                Button(action: {
                                    isSearching = false
                                    searchWord = ""
                                }, label: {
                                    Image(systemName: "xmark.circle.fill")
                                })
                            }
                        }.padding(.horizontal, 20)
                        .foregroundColor(colorPallet.gray)
                    )
                    Spacer()
                    VStack(spacing: 20) {
                        VStack{
                            TabView(selection: $currentIndex){
                                Print("list index")
                                ForEach(data) {row in
                                    if (row.id == 4){
                                        editData()
                                    }else if(row.id == 3){
                                        cartList()
                                    }else if(row.id == 2){
                                        searchData(searchWord: $searchWord, isSearching: $isSearching)
//                                        searchData(searchWord: self.$searchWord, self.$isSearching)
                                    }else if(row.id == 1){
                                        HomeData()
                                    }
                                }
                            }
                            .frame(height: UIScreen.main.bounds.height - 175)
                            Spacer()
                            CustomTapIndecatorHome(count: 4, current: $currentIndex, row: data)
                                .frame(height: 150)
                                .padding(.vertical, -110)
                                .background(colorPallet.orange)
                        }
                    }
                }
            }
            HStack{
                Menu(dark: self.$dark, showMenu: self.$showMenu, showLogin: self.showLogin, showRegister: self.showRegister)
                    .preferredColorScheme(self.dark ? .dark : .light)
                    .offset(x: self.showMenu ? 0 : -UIScreen.main.bounds.width / 1.5)
                
                Spacer()
            }
            
        })
        
    }
    
}

