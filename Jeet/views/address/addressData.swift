//
//  addressData.swift
//  Jeet
//
//  Created by Mac on 8/1/21.
//

import SwiftUI

struct addressData: View {
    @Environment(\.presentationMode) var presentationMode
    @ObservedObject var settings = UserSettings()
    @State var showEdit = false
    @ObservedObject var user = addressObservable()
    var body: some View {
        VStack{
            HStack{
                Spacer()
                Text("عنوان الشحن")
                    .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 15)
                    .lineLimit(1)
                Spacer()
                Button(action: {
                    presentationMode.wrappedValue.dismiss()
                }, label: {
                    Image(systemName: "arrow.backward")
                        .resizable()
                        .frame(width: 20, height: 20)
                        .foregroundColor(colorPallet.gray)
                        .environment(\.layoutDirection, .leftToRight)
                })
                .padding(.trailing, 10)
            }
            .padding()
            .background(colorPallet.bgCard)
            .shadow(radius: 1)
            Spacer()
            ScrollView{
                VStack{
                    HStack{
                        Text("المحافظه")
                            .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 15)
                        Spacer()
                    }
                    
                    Text("")
                            .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 12)
                        .padding(.leading, 20)
                            
                }
                VStack{
                    HStack{
                        Text("الاسم الاول")
                            .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 15)
                        Spacer()
                    }
                    
                    Text(settings.firstName)
                            .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 12)
                        .padding(.leading, 20)
                            
                }
                VStack{
                    HStack{
                        Text("الاسم الثاني")
                            .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 15)
                        Spacer()
                    }
                    
                    Text(settings.lastName)
                            .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 12)
                        .padding(.leading, 20)
                            
                }
                VStack{
                    HStack{
                        Text("الايميل")
                            .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 15)
                        Spacer()
                    }
                    
                    Text(settings.email)
                            .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 12)
                        .padding(.leading, 20)
                            
                }
                
                Button(action: {
                    self.showEdit.toggle()
                }, label: {
                    Text("تعديل البيانات")
                        .padding()
                        .frame(minWidth: 0, maxWidth: .infinity)
                        .background(colorPallet.move)
                        .cornerRadius(10)
                        .chooseFont(fontManager: .BalooBhaijaan, style: .white, weight: .medium, size: 20)
                }).padding()
                .frame(minWidth: 0, maxWidth: .infinity)
                .fullScreenCover(isPresented: $showEdit, content: {
                    addressModify()
                })
            }.padding()
        }
    }
}

struct addressData_Previews: PreviewProvider {
    static var previews: some View {
        addressData()
    }
}
