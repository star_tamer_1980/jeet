//
//  addressModify.swift
//  Jeet
//
//  Created by Mac on 8/1/21.
//

import SwiftUI

struct addressModify: View {
    @Environment(\.presentationMode) var presentationMode
    @ObservedObject var settings = UserSettings()
    @ObservedObject var userObserved = addressObservable()
    @State var userDetails = userModel()
    @State var showMap = false
    @State var showProgress = false
    @State var alertRegisterIsPresented = false
    @State private var latitude = ""
    @State private var longitude = ""
    @State private var address = addressModel()
    @State var addressText = ""
    @State var addressFN = ""
    @State var addressLN = ""
    @State var addressCity = ""
    @State var addressEmail = ""
    var body: some View {
        VStack{
            HStack{
                Spacer()
                Text("تعديل عنوان الشحن")
                    .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 15)
                    .lineLimit(1)
                Spacer()
                Button(action: {
                    presentationMode.wrappedValue.dismiss()
                }, label: {
                    Image(systemName: "arrow.backward")
                        .resizable()
                        .frame(width: 20, height: 20)
                        .foregroundColor(colorPallet.gray)
                        .environment(\.layoutDirection, .leftToRight)
                })
                .padding(.trailing, 10)
            }
            .padding()
            .background(colorPallet.bgCard)
            .shadow(radius: 1)
            Spacer()
            ScrollView{
                VStack{
                    HStack{
                        Button(action: {
                            self.showMap.toggle()
                            settings.addressLatitude = 0.0
                            settings.addressLongitude = 0.0
                        }, label: {
                            TextField("العنوان", text: $addressText)
                                .padding(.leading, 24)
                                .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 12)
                                .disabled(true)
                        }).sheet(isPresented: self.$showMap, content: {
//                                mapDisplayView(showMap: $showMap, address: $address, latitude: $latitude, Longitude: $longitude)
                            mapDisplayView(showMap: $showMap, address: $address, addressText: $addressText, latitude: $latitude, Longitude: $longitude)
                        })

//                            TextField("العنوان", text: $address)
//                                .padding(.leading, 24)
//                                .MonadiFont(style: .black, weight: .medium, size: 12)
                            
                    }.padding(.vertical, 10)
                    .padding(.horizontal, 20)
                    .padding(.leading, -15)
                    .background(colorPallet.bgTextField)
                    .cornerRadius(6)
                    .padding(.horizontal)
                    .overlay(
                        HStack{
                            Spacer()
                            Image(systemName: "location")
                        }.padding(.horizontal, 20)
                        .foregroundColor(colorPallet.gray)
                        
                    )
                    HStack{
                        Text("المحافظه")
                            .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 15)
                        Spacer()
                    }
                    
                    TextField(userDetails.address?.city ?? "", text: $addressCity)
                            .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 12)
                        .padding(.leading, 20)
                            
                }
                VStack{
                    HStack{
                        Text("الاسم الاول")
                            .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 15)
                        Spacer()
                    }
                    
                    TextField(userDetails.address?.fName ?? "", text: $addressFN)
                            .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 12)
                        .padding(.leading, 20)
                            
                }
                VStack{
                    HStack{
                        Text("الاسم الثاني")
                            .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 15)
                        Spacer()
                    }
                    
                    TextField(userDetails.address?.lName ?? "", text: $addressLN)
                            .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 12)
                        .padding(.leading, 20)
                            
                }
                VStack{
                    HStack{
                        Text("الايميل")
                            .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 15)
                        Spacer()
                    }
                    
                    TextField(userDetails.email ?? "", text: $addressEmail)
                            .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 12)
                        .padding(.leading, 20)
                            
                }
                
                Button(action: {
//                    self.showEdit.toggle()
                }, label: {
                    Text("حفظ البيانات")
                        .padding()
                        .frame(minWidth: 0, maxWidth: .infinity)
                        .background(colorPallet.move)
                        .cornerRadius(10)
                        .chooseFont(fontManager: .BalooBhaijaan, style: .white, weight: .medium, size: 20)
                }).padding()
                .frame(minWidth: 0, maxWidth: .infinity)
                .overlay(
                    showProgress ? progress() : nil
                )
                
            }.padding()
            .onAppear(){
                
            }
        }
    }
}

struct addressModify_Previews: PreviewProvider {
    static var previews: some View {
        addressModify()
    }
}
