//
//  authPhoneNumber.swift
//  Jeet
//
//  Created by Mac on 7/28/21.
//

import SwiftUI

struct authPhoneNumber: View {
    @Environment(\.presentationMode) var presentationMode
    @ObservedObject var settings = UserSettings()
    
    @State private var phone = ""
    @State private var alertResult = alertResultModel()
    @State var alertRegisterIsPresented = false
    @State var showOTP = false
    var body: some View {
        VStack{
            HStack{
                Spacer()
                Text("التحقق من رقم الجوال")
                    .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 15)
                    .lineLimit(1)
                Spacer()
                Button(action: {
                    presentationMode.wrappedValue.dismiss()
                }, label: {
                    Image(systemName: "arrow.backward")
                        .resizable()
                        .frame(width: 20, height: 20)
                        .foregroundColor(colorPallet.gray)
                        .environment(\.layoutDirection, .leftToRight)
                })
                .padding(.trailing, 10)
            }
            .padding()
            .background(colorPallet.bgCard)
            .shadow(radius: 1)
            Spacer()
            ScrollView{
                VStack(alignment: .center, spacing: 15){
                    VStack{
                        HStack{
                            TextField("رقم الجوال", text: $phone)
                                .padding(.leading, 24)
                                .disabled(true)
                                .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 12)
                        }.padding(.vertical, 10)
                        .padding(.horizontal, 20)
                        .padding(.leading, -15)
                        .background(colorPallet.bgTextField)
                        .cornerRadius(6)
                        .padding(.horizontal)
                        .overlay(
                            HStack{
                                Spacer()
                                Image(systemName: "person.circle")
                            }.padding(.horizontal, 20)
                            .foregroundColor(colorPallet.gray)
                            
                        )
                        
                     
                        
                    }
                    Button(action: {
                        if(phone == ""){
                            self.alertRegisterIsPresented = true
                            self.alertResult.message = "رقم الجوال غير موجود، نرجوا التاكد من كتابة رقم الجوال بطريقه صحيحه."
                            self.alertResult.status = false
                        }else{
                            self.showOTP = true
                        }
                        
                    }, label: {
                        Text("تحقق")
                            .padding()
                            .boutrosAsmaFont(style: .white, weight: .light, size: 20)
                            .chooseFont(fontManager: .BalooBhaijaan, style: .white, weight: .light, size: 20)
                            .frame(minWidth: 0, maxWidth: .infinity)
                    }).background(colorPallet.move)
                    .cornerRadius(10)
                    .alert(isPresented: $alertRegisterIsPresented, content: {
                        Alert(title: Text("رساله"), message: Text(self.alertResult.message!), dismissButton: .default(Text("OK"), action: {
                            if self.alertResult.status == true{
//                                presentationMode.wrappedValue.dismiss()
                            }
                        }))
                    })
                    .fullScreenCover(isPresented: self.$showOTP, content: {
                        OTPView()
                    })
                    
                    Spacer()
                    
                    
                }
                
                .padding()
                .padding(.top, 20)
                .onAppear(){
                    self.phone = settings.phone
                }
                
            }
        }
    }
}

struct authPhoneNumber_Previews: PreviewProvider {
    static var previews: some View {
        authPhoneNumber()
    }
}
