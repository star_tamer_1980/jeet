//
//  modifyProfile.swift
//  Jeet
//
//  Created by Mac on 7/25/21.
//

import SwiftUI

struct modifyProfile: View {
    @Environment(\.presentationMode) var presentationMode
    @ObservedObject var settings = UserSettings()
    
    @State private var fName = ""
    @State private var lName = ""
    @State private var email = ""
    @State private var phone = ""
    @State private var alertResult = alertResultModel()
    @State var alertRegisterIsPresented = false
    @ObservedObject var data = profileModifireObservable()
    @State private var userData = userModel()
    @State var showProgress = false
    var body: some View {
        VStack{
            HStack{
                Spacer()
                Text("تعديل بيانات عضو")
                    .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 15)
                    .lineLimit(1)
                Spacer()
                Button(action: {
                    presentationMode.wrappedValue.dismiss()
                }, label: {
                    Image(systemName: "arrow.backward")
                        .resizable()
                        .frame(width: 20, height: 20)
                        .foregroundColor(colorPallet.gray)
                        .environment(\.layoutDirection, .leftToRight)
                })
                .padding(.trailing, 10)
            }
            .padding()
            .background(colorPallet.bgCard)
            .shadow(radius: 1)
            Spacer()
            ScrollView{
                VStack(alignment: .center, spacing: 15){
                    VStack{
                        HStack{
                            TextField("الاسم الاول", text: $fName)
                                .padding(.leading, 24)
                                .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 12)
                        }.padding(.vertical, 10)
                        .padding(.horizontal, 20)
                        .padding(.leading, -15)
                        .background(colorPallet.bgTextField)
                        .cornerRadius(6)
                        .padding(.horizontal)
                        .overlay(
                            HStack{
                                Spacer()
                                Image(systemName: "person.circle")
                            }.padding(.horizontal, 20)
                            .foregroundColor(colorPallet.gray)
                            
                        )
                        HStack{
                            TextField("اسم العائله", text: $lName)
                                .padding(.leading, 24)
                                .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 12)
                        }.padding(.vertical, 10)
                        .padding(.horizontal, 20)
                        .padding(.leading, -15)
                        .background(colorPallet.bgTextField)
                        .cornerRadius(6)
                        .padding(.horizontal)
                        .overlay(
                            HStack{
                                Spacer()
                                Image(systemName: "person.circle")
                            }.padding(.horizontal, 20)
                            .foregroundColor(colorPallet.gray)
                            
                        )
                        
                        HStack{
                            TextField("البريد الالكتروني", text: $email)
                                .padding(.leading, 24)
                                .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 12)
                        }.padding(.vertical, 10)
                        .padding(.horizontal, 20)
                        .padding(.leading, -15)
                        .background(colorPallet.bgTextField)
                        .cornerRadius(6)
                        .padding(.horizontal)
                        .overlay(
                            HStack{
                                Spacer()
                                Image(systemName: "envelope")
                            }.padding(.horizontal, 20)
                            .foregroundColor(colorPallet.gray)
                            
                        )
                     
                        
                    }
                    Button(action: {
                        showProgress = true
                        userData.fName = self.fName
                        userData.lName = self.lName
                        userData.email = self.email
                        userData.phone = settings.phone
                        userData.token = settings.token
                        data.sendData(userData) { (status, message) in
                            self.showProgress = false
                            self.alertRegisterIsPresented = true
                            self.alertResult.status = status
                            self.alertResult.message = message
                            if(status == true){
                                settings.firstName = self.fName
                                settings.lastName = self.lName
                                settings.email = self.email
                            }
                        }

                        
                    }, label: {
                        Text("تحديث البيانات")
                            .padding()
                            .boutrosAsmaFont(style: .white, weight: .light, size: 20)
                            .chooseFont(fontManager: .BalooBhaijaan, style: .white, weight: .light, size: 20)
                            .frame(minWidth: 0, maxWidth: .infinity)
                    }).background(colorPallet.move)
                    .cornerRadius(10)
                    .alert(isPresented: $alertRegisterIsPresented, content: {
                        Alert(title: Text("رساله"), message: Text(self.alertResult.message!), dismissButton: .default(Text("OK"), action: {
                            if self.alertResult.status == true{
//                                presentationMode.wrappedValue.dismiss()
                            }
                        }))
                    })
                    
                    Spacer()
                    
                    
                }
                .padding()
                .padding(.top, 20)
                .overlay(
                    showProgress ? progress() : nil
                )
                .onAppear(){
                    self.fName = settings.firstName
                    self.lName = settings.lastName
                    self.email = settings.email
                }
                
            }
        }
    }
}

struct modifyProfile_Previews: PreviewProvider {
    static var previews: some View {
        modifyProfile()
    }
}
