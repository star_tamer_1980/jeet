//
//  editData.swift
//  Jeet
//
//  Created by Mac on 5/25/21.
//

import SwiftUI

struct editData: View {
    @Environment(\.presentationMode) var presentationMode
    @ObservedObject var settings = UserSettings()
    @State var showEdit = false
    var body: some View {
        ScrollView{
            VStack{
                HStack{
                    Text("اسم العضو: ")
                        .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 15)
                    Text("اسم العرض")
                        .chooseFont(fontManager: .BalooBhaijaan, style: .gray, weight: .medium, size: 13)
                    Spacer()
                }
                
                Text(settings.displayName)
                        .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 12)
                    .padding(.leading, 20)
                        
            }
            VStack{
                HStack{
                    Text("الاسم الاول")
                        .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 15)
                    Spacer()
                }
                
                Text(settings.firstName)
                        .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 12)
                    .padding(.leading, 20)
                        
            }
            VStack{
                HStack{
                    Text("الاسم الثاني")
                        .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 15)
                    Spacer()
                }
                
                Text(settings.lastName)
                        .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 12)
                    .padding(.leading, 20)
                        
            }
            VStack{
                HStack{
                    Text("الايميل")
                        .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 15)
                    Spacer()
                }
                
                Text(settings.email)
                        .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 12)
                    .padding(.leading, 20)
                        
            }
            VStack{
                HStack{
                    Text("رقم الهاتف")
                        .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 15)
                    Spacer()
                }
                
                Text(settings.phone)
                        .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 12)
                    .padding(.leading, 20)
                        
            }
            Button(action: {
                self.showEdit.toggle()
            }, label: {
                Text("تعديل البيانات")
                    .padding()
                    .frame(minWidth: 0, maxWidth: .infinity)
                    .background(colorPallet.move)
                    .cornerRadius(10)
                    .chooseFont(fontManager: .BalooBhaijaan, style: .white, weight: .medium, size: 20)
            }).padding()
            .frame(minWidth: 0, maxWidth: .infinity)
            .fullScreenCover(isPresented: $showEdit, content: {
                modifyProfile()
            })
        }.padding()
    }
}

struct editData_Previews: PreviewProvider {
    static var previews: some View {
        editData()
    }
}
