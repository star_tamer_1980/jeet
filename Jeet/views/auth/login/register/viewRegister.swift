//
//  viewRegister.swift
//  Jeet
//
//  Created by Mac on 6/17/21.
//

import SwiftUI

struct viewRegister: View {
//    @State private var fName = "aa"
//    @State private var lName = "aa"
//    @State private var username = "aa"
//    @State private var email = "aa@aa.com"
//    @State private var phone = "0512315555"
//    @State private var password = "123456789"
//    @State private var passwordCurrentHide = true
//    @State private var rePassword = "123456789"
//    @State private var rePasswordCurrentHide = true
    
    @State private var fName = ""
    @State private var lName = ""
    @State private var address = addressModel()
    @State private var addressText = ""
    @State private var latitude = ""
    @State private var longitude = ""
    @State private var username = ""
    @State private var email = ""
    @State private var phone = ""
    @State private var password = ""
    @State private var passwordCurrentHide = true
    @State private var rePassword = ""
    @State private var rePasswordCurrentHide = true
    @State var showProgress = false
    @State var alertRegisterIsPresented = false
    @State private var alertResult = alertResultModel()
    @State var showMap = false
    @State var showOTP = false
    
    @ObservedObject var settings = UserSettings()
    @ObservedObject var register = registerObservable()
    @ObservedObject var profile = profileObservable()
    @State private var userData = userModel()
    @Environment(\.presentationMode) var presentationMode
    var body: some View {
        VStack{
            HStack{
                Spacer()
                Text("تسجيل عضو")
                    .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 15)
                    .lineLimit(1)
                Spacer()
                Button(action: {
                    presentationMode.wrappedValue.dismiss()
                }, label: {
                    Image(systemName: "arrow.backward")
                        .resizable()
                        .frame(width: 20, height: 20)
                        .foregroundColor(colorPallet.gray)
                        .environment(\.layoutDirection, .leftToRight)
                })
                .padding(.trailing, 10)
            }
            .padding()
            .background(colorPallet.bgCard)
            .shadow(radius: 1)
            Spacer()
            ScrollView{
                VStack(alignment: .center, spacing: 15){
                    Group{
                        Text("تسجيل عضويه")
                            .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .black, size: 22)
                        Text("قم بتسجيل عضويه , حتي تسطيع التسوق من خلال التطبيق ومتباعه جديد المنتجات")
                            .chooseFont(fontManager: .BalooBhaijaan, style: .gray, weight: .light, size: 18)
                    }
                    VStack{
                        HStack{
                            TextField("الاسم الاول", text: $fName)
                                .padding(.leading, 24)
                                .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 12)
                        }.padding(.vertical, 10)
                        .padding(.horizontal, 20)
                        .padding(.leading, -15)
                        .background(colorPallet.bgTextField)
                        .cornerRadius(6)
                        .padding(.horizontal)
                        .overlay(
                            HStack{
                                Spacer()
                                Image(systemName: "person.circle")
                            }.padding(.horizontal, 20)
                            .foregroundColor(colorPallet.gray)
                            
                        )
                        HStack{
                            TextField("اسم العائله", text: $lName)
                                .padding(.leading, 24)
                                .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 12)
                        }.padding(.vertical, 10)
                        .padding(.horizontal, 20)
                        .padding(.leading, -15)
                        .background(colorPallet.bgTextField)
                        .cornerRadius(6)
                        .padding(.horizontal)
                        .overlay(
                            HStack{
                                Spacer()
                                Image(systemName: "person.circle")
                            }.padding(.horizontal, 20)
                            .foregroundColor(colorPallet.gray)
                            
                        )
                        HStack{
                            Button(action: {
                                self.showMap.toggle()
                                settings.addressLatitude = 0.0
                                settings.addressLongitude = 0.0
                            }, label: {
                                TextField("العنوان", text: $addressText)
                                    .padding(.leading, 24)
                                    .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 12)
                                    .disabled(true)
                            }).sheet(isPresented: self.$showMap, content: {
//                                mapDisplayView(showMap: $showMap, address: $address, latitude: $latitude, Longitude: $longitude)
                                mapDisplayView(showMap: $showMap, address: $address, addressText: $addressText, latitude: $latitude, Longitude: $longitude)
                            })

//                            TextField("العنوان", text: $address)
//                                .padding(.leading, 24)
//                                .MonadiFont(style: .black, weight: .medium, size: 12)
                                
                        }.padding(.vertical, 10)
                        .padding(.horizontal, 20)
                        .padding(.leading, -15)
                        .background(colorPallet.bgTextField)
                        .cornerRadius(6)
                        .padding(.horizontal)
                        .overlay(
                            HStack{
                                Spacer()
                                Image(systemName: "location")
                            }.padding(.horizontal, 20)
                            .foregroundColor(colorPallet.gray)
                            
                        )
                        HStack{
                            TextField("اسم المستخدم", text: $username)
                                .padding(.leading, 24)
                                .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 12)
                        }.padding(.vertical, 10)
                        .padding(.horizontal, 20)
                        .padding(.leading, -15)
                        .background(colorPallet.bgTextField)
                        .cornerRadius(6)
                        .padding(.horizontal)
                        .overlay(
                            HStack{
                                Spacer()
                                Image(systemName: "person.2")
                            }.padding(.horizontal, 20)
                            .foregroundColor(colorPallet.gray)
                            
                        )
                        HStack{
                            TextField("البريد الالكتروني", text: $email)
                                .padding(.leading, 24)
                                .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 12)
                        }.padding(.vertical, 10)
                        .padding(.horizontal, 20)
                        .padding(.leading, -15)
                        .background(colorPallet.bgTextField)
                        .cornerRadius(6)
                        .padding(.horizontal)
                        .overlay(
                            HStack{
                                Spacer()
                                Image(systemName: "envelope")
                            }.padding(.horizontal, 20)
                            .foregroundColor(colorPallet.gray)
                            
                        )
                        
                        HStack{
                            TextField("الهاتف", text: $phone)
                                .padding(.leading, 24)
                                .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 12)
                        }.padding(.vertical, 10)
                        .padding(.horizontal, 20)
                        .padding(.leading, -15)
                        .background(colorPallet.bgTextField)
                        .cornerRadius(6)
                        .padding(.horizontal)
                        .overlay(
                            HStack{
                                Spacer()
                                Image(systemName: "iphone.homebutton")
                            }.padding(.horizontal, 20)
                            .foregroundColor(colorPallet.gray)
                            
                        )
                        if self.passwordCurrentHide{
                            HStack{
                                SecureField("كلمة السر", text: $password)
                                    
                                    .padding(.leading, 24)
                                    .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 12)
                            }.padding(.vertical, 10)
                            .padding(.horizontal, 20)
                            .padding(.leading, -15)
                            .background(colorPallet.bgTextField)
                            .cornerRadius(6)
                            .padding(.horizontal)
                            .overlay(
                                HStack{
                                    Spacer()
                                    Button(action: {
                                        self.passwordCurrentHide.toggle()
                                    }, label: {
                                        Image(systemName: "eye")
                                    })
                                    
                                }.padding(.horizontal, 20)
                                .foregroundColor(colorPallet.gray)
                                
                            )
                        }else{
                            HStack{
                                TextField("كلمة السر", text: $password)
                                    
                                    .padding(.leading, 24)
                                    .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 12)
                            }.padding(.vertical, 10)
                            .padding(.horizontal, 20)
                            .padding(.leading, -15)
                            .background(colorPallet.bgTextField)
                            .cornerRadius(6)
                            .padding(.horizontal)
                            .overlay(
                                HStack{
                                    Spacer()
                                    Button(action: {
                                        self.passwordCurrentHide.toggle()
                                    }, label: {
                                        Image(systemName: "eye.slash")
                                    })
                                    
                                }.padding(.horizontal, 20)
                                .foregroundColor(colorPallet.gray)
                                
                            )
                        }
                        
                        if self.rePasswordCurrentHide{
                            HStack{
                                SecureField("اعادة كلمة السر", text: $rePassword)
                                    
                                    .padding(.leading, 24)
                                    .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 12)
                            }.padding(.vertical, 10)
                            .padding(.horizontal, 20)
                            .padding(.leading, -15)
                            .background(colorPallet.bgTextField)
                            .cornerRadius(6)
                            .padding(.horizontal)
                            .overlay(
                                HStack{
                                    Spacer()
                                    Button(action: {
                                        self.rePasswordCurrentHide.toggle()
                                    }, label: {
                                        Image(systemName: "eye")
                                    })
                                    
                                }.padding(.horizontal, 20)
                                .foregroundColor(colorPallet.gray)
                                
                            )
                        }else{
                            HStack{
                                TextField("كلمة السر", text: $rePassword)
                                    
                                    .padding(.leading, 24)
                                    .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 12)
                            }.padding(.vertical, 10)
                            .padding(.horizontal, 20)
                            .padding(.leading, -15)
                            .background(colorPallet.bgTextField)
                            .cornerRadius(6)
                            .padding(.horizontal)
                            .overlay(
                                HStack{
                                    Spacer()
                                    Button(action: {
                                        self.rePasswordCurrentHide.toggle()
                                    }, label: {
                                        Image(systemName: "eye.slash")
                                    })
                                    
                                }.padding(.horizontal, 20)
                                .foregroundColor(colorPallet.gray)
                                
                            )
                        }
                        
                        HStack{
                            Text("عند التسجيل في المتجر فانت توافق على")
                                .chooseFont(fontManager: .BalooBhaijaan, style: .gray, weight: .medium, size: 10)
                            Text("الشروط والاحكام")
                                .chooseFont(fontManager: .BalooBhaijaan, style: .gray, weight: .medium, size: 10)
                        }
                        
                    }
                    Button(action: {
                        if(self.password == self.rePassword){
                            showProgress = true
                            userData.fName = self.fName
                            userData.lName = self.lName
                            userData.username = self.username
                            userData.email = self.email
                            userData.phone = self.phone
                            userData.password = self.password
                                register.sendData(userData) { (data,statue, message) in
                                    self.alertRegisterIsPresented = true
                                    self.alertResult.status = statue
                                    self.alertResult.message = message
                                    if(statue == true){
                                        settings.token = (data?.token)!
                                        settings.displayName = (data?.displayName)!
                                        settings.nicename = (data?.niceName)!
//                                        settings.email = (data?.email)!
                                        settings.isLogin = (data?.isLogin)!
//                                        settings.phone = (data?.phone)!
                                        profile.details(settings.token) { (userData, error) in
                                            showProgress = false
                                            if(error == ""){
                                                settings.email = userData?.email ?? ""
                                                settings.phone = userData?.phone ?? ""
                                                settings.firstName = userData?.fName ?? ""
                                                settings.lastName = userData?.lName ?? ""
                                                settings.username = userData?.username ?? ""
                                                settings.phoneVerify = false
                                            }
                                            self.showOTP.toggle()
                                        }
                                    }
                                }
                        }else{
                            self.alertRegisterIsPresented = true
                            self.alertResult.status = false
                            self.alertResult.message = "Passwored not match rePassword"
                        }
                        
                        
                    }, label: {
                        Text("تسجيل")
                            .padding()
                            .boutrosAsmaFont(style: .white, weight: .light, size: 20)
                            .chooseFont(fontManager: .BalooBhaijaan, style: .white, weight: .light, size: 20)
                            .frame(minWidth: 0, maxWidth: .infinity)
                    }).background(colorPallet.move)
                    .cornerRadius(10)
//                    .alert(isPresented: $alertRegisterIsPresented, content: {
//                        Alert(title: Text("رساله"), message: Text(self.alertResult.message!), dismissButton: .default(Text("OK"), action: {
//                            if self.alertResult.status == true{
////                                presentationMode.wrappedValue.dismiss()
//                                OTPView()
//                            }
//                        }))
//                    })
                    .fullScreenCover(isPresented: self.$showOTP, content: {
                        OTPView()
                    })
                    Spacer()
                    Divider()
                    Text("او")
                        .chooseFont(fontManager: .BalooBhaijaan, style: .gray, weight: .medium, size: 20)
                        .padding()
                        .background(colorPallet.white)
                        .padding(.top, -45)
                    
                    Button(action: {}, label: {
                        HStack{
                            
                            Image("facebook")
                                .padding()
                            Spacer()
                            Text("فيسبوك")
                                .padding()
                                .chooseFont(fontManager: .BalooBhaijaan, style: .white, weight: .light, size: 20)
                            Spacer()
                        }.frame(minWidth: 0, maxWidth: .infinity)
                    }).background(colorPallet.buttonFacebookBlue)
                    .cornerRadius(10)
                    
                    Button(action: {}, label: {
                        HStack{
                            
                            Image("google")
                                .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 20)
                                .padding()
                            Spacer()
                            Text("جوجل")
                                .padding()
                                .chooseFont(fontManager: .BalooBhaijaan, style: .white, weight: .light, size: 20)
                            Spacer()
                        }.frame(minWidth: 0, maxWidth: .infinity)
                    }).background(colorPallet.buttonGoogleRed)
                    .cornerRadius(10)
                    
                    Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/, label: {
                        HStack{
                            
                            Image("apple")
                                .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .light, size: 20)
                                .padding()
                            Spacer()
                            Text("ابل")
                                .padding()
                                .chooseFont(fontManager: .BalooBhaijaan, style: .white, weight: .light, size: 20)
                            Spacer()
                        }.frame(minWidth: 0, maxWidth: .infinity)
                    }).background(colorPallet.black)
                    .cornerRadius(10)
                    
                }
                .padding()
                .padding(.top, 20)
                .overlay(
                    showProgress ? progress() : nil
                )
                
            }
        }
        
    }
}

struct viewRegister_Previews: PreviewProvider {
    static var previews: some View {
        viewRegister()
    }
}
