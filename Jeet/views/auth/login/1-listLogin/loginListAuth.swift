//
//  loginListAuth.swift
//  Jeet
//
//  Created by Mac on 5/29/21.
//

import SwiftUI



struct loginListAuth: View {
    @State private var email = ""
    @State private var password = ""
    @State private var passwordCurrentHide = true
    @State private var btnReg = false
    @State var showProgress = false
    @State var alertLoginIsPresented = false
    @State private var alertResult = alertResultModel()
    
    @ObservedObject var settings = UserSettings()
    @ObservedObject var login = loginObserval()
    @ObservedObject var profile = profileObservable()
    @State private var userData = userModel()
    @Environment(\.presentationMode) var presentationMode
    var body: some View {
        ScrollView{
            VStack{
                HStack{
                    Spacer()
                    Text("تسجيل دخول عضو")
                        .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 15)
                        .lineLimit(1)
                    Spacer()
                    Button(action: {
                        
                        presentationMode.wrappedValue.dismiss()
                    }, label: {
                        Image(systemName: "arrow.backward")
                            .resizable()
                            .frame(width: 20, height: 20)
                            .foregroundColor(colorPallet.gray)
                            .environment(\.layoutDirection, .leftToRight)
                    })
                    .padding(.trailing, 10)
                }
                .padding()
                .background(colorPallet.bgCard)
                .shadow(radius: 1)
                Spacer()
                ScrollView{
                    VStack(alignment: .center, spacing: 15){
                        Group{
                            Text("تسجيل دخول")
                                .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .black, size: 22)
                            Text("قم بتسجيل الدخول , حتي تسطيع التسوق من خلال التطبيق ومتباعه جديد المنتجات")
                                .boutrosAsmaFont(style: .gray, weight: .light, size: 18)
                        }
                        VStack{
                            HStack{
                                TextField("البريد الالكتروني", text: $email)
                                    .padding(.leading, 24)
                                    .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 12)
                            }.padding(.vertical, 10)
                            .padding(.horizontal, 20)
                            .padding(.leading, -15)
                            .background(colorPallet.bgTextField)
                            .cornerRadius(6)
                            .padding(.horizontal)
                            .overlay(
                                HStack{
                                    Spacer()
                                    Image(systemName: "person.circle")
                                }.padding(.horizontal, 20)
                                .foregroundColor(colorPallet.gray)
                                
                            )
                            if self.passwordCurrentHide{
                                HStack{
                                    SecureField("كلمة السر", text: $password)
                                        .padding(.leading, 24)
                                        .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 12)
                                }.padding(.vertical, 10)
                                .padding(.horizontal, 20)
                                .padding(.leading, -15)
                                .background(colorPallet.bgTextField)
                                .cornerRadius(6)
                                .padding(.horizontal)
                                .overlay(
                                    HStack{
                                        Spacer()
                                        Button(action: {
                                            self.passwordCurrentHide.toggle()
                                        }, label: {
                                            Image(systemName: "eye")
                                        })
                                        
                                    }.padding(.horizontal, 20)
                                    .foregroundColor(colorPallet.gray)
                                    
                                )
                            }else{
                                HStack{
                                    TextField("كلمة السر", text: $password)
                                        .padding(.leading, 24)
                                        .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 12)
                                }.padding(.vertical, 10)
                                .padding(.horizontal, 20)
                                .padding(.leading, -15)
                                .background(colorPallet.bgTextField)
                                .cornerRadius(6)
                                .padding(.horizontal)
                                .overlay(
                                    HStack{
                                        Spacer()
                                        Button(action: {
                                            self.passwordCurrentHide.toggle()
                                        }, label: {
                                            Image(systemName: "eye.slash")
                                        })
                                        
                                    }.padding(.horizontal, 20)
                                    .foregroundColor(colorPallet.gray)
                                    
                                )
                            }
                            
                            
                            Button(action: {
                                showProgress = true
                                userData.email = self.email
                                userData.password = self.password
                                login.login(userData) { (data,statue, message) in
                                        self.alertLoginIsPresented = true
                                        self.alertResult.status = statue
                                        self.alertResult.message = message
                                        if(statue == true){
                                            settings.token = (data?.token)!
                                            settings.displayName = (data?.displayName)!
                                            settings.nicename = (data?.niceName)!
                                            settings.isLogin = (data?.isLogin)!
                                            profile.details(settings.token) { (userData, error) in
                                                showProgress = false
                                                if(error == ""){
                                                    settings.email = userData?.email ?? ""
                                                    settings.phone = userData?.phone ?? ""
                                                    settings.firstName = userData?.fName ?? ""
                                                    settings.lastName = userData?.lName ?? ""
                                                    settings.username = userData?.username ?? ""
                                                    settings.phoneVerify = false
                                                    
                                                    print("settings email \(settings.email) - user email \(userData?.email)")
                                                    print("settings phone \(settings.phone) - user email \(userData?.phone)")
                                                    print("settings firstName \(settings.firstName) - user email \(userData?.fName)")
                                                    print("settings lastName \(settings.lastName) - user email \(userData?.lName)")
                                                    print("settings username \(settings.username) - user email \(userData?.username)")
                                                }
                                                
                                            }
                                        }
                                        print("status is: \(String(describing: settings.isLogin)) , message is : \(settings.token)")
                                    }
                            }, label: {
                                HStack(alignment: .center){
                                    
                                    Text("دخول")
                                        .padding()
                                        .chooseFont(fontManager: .BalooBhaijaan, style: .white, weight: .light, size: 20)
                                }.frame(minWidth: 0, maxWidth: .infinity)
                            }).background(colorPallet.buttonFacebookBlue)
                            .cornerRadius(10)
                            .alert(isPresented: $alertLoginIsPresented, content: {
                                Alert(title: Text("رساله"), message: Text(self.alertResult.message!), dismissButton: .default(Text("OK"), action: {
                                    if self.alertResult.status == true{
                                        presentationMode.wrappedValue.dismiss()
                                    }
                                }))
                            })
                            
                        }
                        Spacer()
                        Divider()
                        Text("او")
                            .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .black, size: 20)
                            .padding()
                            .background(colorPallet.white)
                            .padding(.top, -45)
                        
                        Button(action: {}, label: {
                            HStack{
                                
                                Image("facebook")
                                    .padding()
                                Spacer()
                                Text("فيسبوك")
                                    .padding()
                                    .chooseFont(fontManager: .BalooBhaijaan, style: .white, weight: .black, size: 20)
                                Spacer()
                            }.frame(minWidth: 0, maxWidth: .infinity)
                        }).background(colorPallet.buttonFacebookBlue)
                        .cornerRadius(10)
                        
                        Button(action: {}, label: {
                            HStack{
                                
                                Image("google")
                                    .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 20)
                                    .padding()
                                Spacer()
                                Text("جوجل")
                                    .padding()
                                    .chooseFont(fontManager: .BalooBhaijaan, style: .white, weight: .light, size: 20)
                                Spacer()
                            }.frame(minWidth: 0, maxWidth: .infinity)
                        }).background(colorPallet.buttonGoogleRed)
                        .cornerRadius(10)
                        
                        Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/, label: {
                            HStack{
                                
                                Image("apple")
                                    .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 20)
                                    .padding()
                                Spacer()
                                Text("ابل")
                                    .padding()
                                    .chooseFont(fontManager: .BalooBhaijaan, style: .white, weight: .light, size: 20)
                                Spacer()
                            }.frame(minWidth: 0, maxWidth: .infinity)
                        }).background(colorPallet.black)
                        .cornerRadius(10)
                        
                        HStack(spacing: 20){
                            Text("ليس لديك حساب؟")
                                .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 20)
                            Button(action: {
                                self.btnReg.toggle()
                            }, label: {
                                Text("تسجيل حساب")
                                    .chooseFont(fontManager: .BalooBhaijaan, style: .move, weight: .medium, size: 20)
                            }).fullScreenCover(isPresented: $btnReg, content: {
                                viewRegister()
                            })
                            
                        }
                    }
                    .overlay(
                        showProgress ? progress() : nil
                    )
                    .padding()
                    .padding(.top, 20)
                    
                }
                
            }
        }
        
    }
}

struct loginListAuth_Previews: PreviewProvider {
    static var previews: some View {
        loginListAuth()
    }
}



