
////
//OTPView.swift
//PabdaOTP
//
//Created by Basel Baragabah on 02/03/2021.
//Copyright © 2021 Basel Baragabah. All rights reserved.
//
import SwiftUI

struct OTPView: View {
    // 5 minutes
    @State private var timeRemaining = 300
    @State private var isOtpMatching = false
    @ObservedObject var send = otbObserval()
    @ObservedObject var settings = UserSettings()
    @State private var alertResult = alertResultModel()
    @State var alertRegisterIsPresented = false
    
    @State var isOTP = false

    let timer = Timer.publish(every: 1, on: .main, in: .common).autoconnect()
 
    init() {
        
        send.send(settings.phone, settings.token) { (status, error) in
            print("status \(String(describing: status))")
        }
    }
    var body: some View {
        VStack {
            
            HStack {
                Text("كود التحقق")
                    .font(.title2)
                    .fontWeight(.medium)
                    .foregroundColor(.green)
                Spacer()
            }
            .padding(.leading)
            .onAppear(){
                
            }
            
            HStack {
                Text("لقد تم ارسال كود علي رقم الجوال الخاص بك، لو سمحت اكتب الارقام في الخانات المخصصه.")
                    .font(.body)
                Spacer()
            }
            .padding(.leading)
            .padding(.top,5)
            .padding(.bottom, 30)
            
            VStack {
                OTPTextFieldView { otp, completionHandler in

                    print("otp = \(otp)")
                    send.verify(otp, settings.token) { (status, error) in
                        isOTP = status!
                        settings.phoneVerify = status!
                        alertResult.status = status!
                        alertResult.message = error
                    }
//                    if otp == "1234" { // this could be a network call
//                        completionHandler(true)
//                        isOtpMatching = true
//
//                    } else {
//                        completionHandler(false)
//                        isOtpMatching = false
//                    }

                }
                
                
                Text(getTimer())
                    .padding()
                    .onReceive(timer) { _ in
                        if timeRemaining > 0 {
                            timeRemaining -= 1
                        }
                    }
                
                Button(action: {
                    timeRemaining = 300
                }, label: {
                    Text("اعادة ارسال الكود")
                        .font(.callout)
                        .foregroundColor(.white)
                })
                .padding()
                .frame(maxWidth: .infinity)
                .background(Color.green)
                .clipShape(RoundedRectangle(cornerRadius: 5))
                .padding(.horizontal)
                
            }.alert(isPresented: $alertRegisterIsPresented, content: {
                Alert(title: Text("رساله"), message: Text(self.alertResult.message!), dismissButton: .default(Text("OK"), action: {
                    
                }))
            })
            
        }.fullScreenCover(isPresented: $isOTP, content: {
            if(isOTP == true){
                HomeView()
            }
        })
        .alert(isPresented: $alertRegisterIsPresented, content: {
            Alert(title: Text("رساله"), message: Text(self.alertResult.message!), dismissButton: .default(Text("OK"), action: {
                
            }))
        })
        
    }
    
    func getTimer() -> String{
        let minutes = Int(timeRemaining) / 60 % 60
        let seconds = Int(timeRemaining) % 60
        return String(format:"%02i:%02i", minutes, seconds)
    }
}
