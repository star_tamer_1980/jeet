//
//  addressShipping.swift
//  Jeet
//
//  Created by Mac on 6/14/21.
//

import SwiftUI
import Kingfisher
import ToastUI

struct addressShipping: View {
    var productData = ""
    @ObservedObject var shipping = cartObserval()
    @State private var showProgress = true
    @State var showShipping = false
    
    @State var payNow = false
    @StateObject var products = productCartCoreDataObservable()
    @Environment(\.presentationMode) var presentationMode
    @ObservedObject var settings = UserSettings()
    
    let coreDM = CoreDataManager()
    
    
    @State private var showToast = false
    @ObservedObject var addCart = addCartAdapter()
    @State var addToCartSuccess = false
    
    var body: some View {
        VStack{
            HStack{
                Spacer()
                Text("تفاصيل الدفع")
                    .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 15)
                    .lineLimit(1)
                Spacer()
                Button(action: {
                    presentationMode.wrappedValue.dismiss()
                }, label: {
                    Image(systemName: "arrow.backward")
                        .resizable()
                        .frame(width: 20, height: 20)
                        .foregroundColor(colorPallet.gray)
                        .environment(\.layoutDirection, .leftToRight)
                })
                .padding(.trailing, 10)
            }
            .padding()
            .background(colorPallet.bgCard)
            .shadow(radius: 1)
            
            
            List{
                ForEach(shipping.list, id: \.self){ ship in
                    Button(action: {
                        self.payNow.toggle()
                        self.showProgress = true
                        print("list \(products.calculateProducts())")
                        self.showToast = true
                        self.addCart.addProductToCart(token: settings.token, products: products.calculateProducts()) {
                            self.showProgress = false
                            products.list.forEach { (product) in
                                products.deleteProduct(product: product)
                            }
                        }
                    }, label: {
//                        Text(ship.title!)
                        KFImage(URL(string: (ship.icon != "") ? ship.icon! : "https://jeet.codlop.com/wp-content/uploads/2021/05/a-564-300x300.jpg"))
                            .resizable()
                            .scaledToFit()
                            .frame(width: UIScreen.main.bounds.width - 30, height: 100)
                    })
//                    .fullScreenCover(isPresented: self.$payNow, content: {
//
//                    })
                    .padding()

                }
            }
            .toast(isPresented: $showToast, dismissAfter: 3){
            } content: {
                ToastView("تم الاضافه ")
            }
            
            VStack{
                VStack{
                    Text("الاسم الاول")
                        .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 15)
                        .frame(maxWidth: .infinity, alignment: .leading)
                    Text(String(shipping.data.address?.billingFirstName ?? ""))
                        .chooseFont(fontManager: .BalooBhaijaan, style: .gray, weight: .medium, size: 13)
                        .padding(.leading, 50)
                        .frame(maxWidth: .infinity, alignment: .leading)
                }
                VStack{
                    Text("الاسم الثاني")
                        .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 15)
                        .frame(maxWidth: .infinity, alignment: .leading)
                    Text(String(shipping.data.address?.billingLastName ?? ""))
                        .chooseFont(fontManager: .BalooBhaijaan, style: .gray, weight: .medium, size: 13)
                        .padding(.leading, 50)
                        .frame(maxWidth: .infinity, alignment: .leading)
                }
                VStack{
                    Text("الايميل")
                        .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 15)
                        .frame(maxWidth: .infinity, alignment: .leading)
                    Text(String(shipping.data.address?.billingEmail ?? ""))
                        .chooseFont(fontManager: .BalooBhaijaan, style: .gray, weight: .medium, size: 13)
                        .padding(.leading, 50)
                        .frame(maxWidth: .infinity, alignment: .leading)
                }
                VStack{
                    Text("المدينه")
                        .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 15)
                        .frame(maxWidth: .infinity, alignment: .leading)
                    Text(String(shipping.data.address?.billingGovernorate ?? ""))
                        .padding(.leading, 50)
                        .frame(maxWidth: .infinity, alignment: .leading)
                        .chooseFont(fontManager: .BalooBhaijaan, style: .gray, weight: .medium, size: 13)
                }
                VStack{
                    Text("المنطقه")
                        .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 15)
                        .frame(maxWidth: .infinity, alignment: .leading)
                    Text(String(shipping.data.address?.billingCountry ?? ""))
                        .chooseFont(fontManager: .BalooBhaijaan, style: .gray, weight: .medium, size: 13)
                        .padding(.leading, 50)
                        .frame(maxWidth: .infinity, alignment: .leading)
                }
                VStack{
                    Text("المدينه")
                        .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 15)
                        .frame(maxWidth: .infinity, alignment: .leading)
                    Text(String(shipping.data.address?.billingCity ?? ""))
                        .chooseFont(fontManager: .BalooBhaijaan, style: .gray, weight: .medium, size: 13)
                        .padding(.leading, 50)
                        .frame(maxWidth: .infinity, alignment: .leading)
                }
                VStack{
                    Text("العنوان")
                        .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 15)
                        .frame(maxWidth: .infinity, alignment: .leading)
                    Text(String(shipping.data.address?.billingAddress1 ?? ""))
                        .chooseFont(fontManager: .BalooBhaijaan, style: .gray, weight: .medium, size: 13)
                        .padding(.leading, 50)
                        .frame(maxWidth: .infinity, alignment: .leading)
                }
            }
            .padding()
            
            Button(action: {
                self.showShipping.toggle()
            }, label: {
                Text("تعديل بيانات العنوان")
                    .MonadiFont(style: .white, weight: .black, size: 22)
                    .chooseFont(fontManager: .BalooBhaijaan, style: .white, weight: .black, size: 22)
                    .multilineTextAlignment(.center)
                
            }).fullScreenCover(isPresented: self.$showShipping, content: {
//                addressShipping()
            })
            .padding()
            .cornerRadius(10)
            .background(colorPallet.move)
            .frame(width: 400, alignment: .center)
            Spacer()
            
        }
        .overlay(
            showProgress ? progress() : nil
        )
        .onAppear(){
            DispatchQueue.main.async {
                showProgress = true
                products.getAllData{
                    print("product is: \(products.calculateProducts())")
                    shipping.getShippingData(token: settings.token, products: products.calculateProducts(), completion: {
                        showProgress = false
                    })
                }
            }
            
            
        }
        
        Spacer()
    }
        
    func removeCart(){
//        ForEach(products.list, id: \.self){ product in
//            products.deleteProduct(product: product)
//
//        }
        products.list.forEach { (product) in
            products.deleteProduct(product: product)
        }
    }
}

struct addressShipping_Previews: PreviewProvider {
    static var previews: some View {
        addressShipping()
    }
}
