//
//  cartList.swift
//  Jeet
//
//  Created by Mac on 5/25/21.
//

import SwiftUI
import CoreData

struct cartList: View {
    
    @State var showShipping = false
    @State var productDetailsId = 0
    let coreDM = CoreDataManager()
    
//    @StateObject var products = productCartCoreModel()
    @StateObject var products = productCartCoreDataObservable()
    var body: some View {
        NavigationView(){
            VStack(alignment: .leading, spacing: 2){
                HStack(spacing: 5){
                    ZStack{
                        Image(systemName: "cart.fill")
                            .boutrosAsmaFont(style: .move, weight: .medium, size: 22)
                          .padding(10)
                        ZStack{
                          Circle()
                            .stroke(colorPallet.gray, lineWidth: 1)
                            .background(Circle().fill(colorPallet.orange))
                            .padding(1)
                              .frame(width: 15, height: 15)
                            Text("\(products.list.count)")
                                .chooseFont(fontManager: .BalooBhaijaan, style: .white, weight: .medium, size: 12)
                              .padding(5)
                        }.padding(.bottom, 10)
                        .padding(.leading, 10)
                    }
                    Text("يوجد لديك \(products.list.count) منتجات بالسله")
                        .chooseFont(fontManager: .BalooBhaijaan, style: .gray, weight: .medium, size: 15)
                }
                
                List{
                    ForEach(products.list){ product in
                        productCartCard(product: $products.list[getIndex(product: product)])

                    }.onDelete { indexSet in
                        indexSet.forEach{ index in
                            let product = products.list[index]
                            products.deleteProduct(product: product)
                            products.getAllData(){
                                
                            }
                        }
                    }
                }
                .onAppear(){
                    DispatchQueue.main.async {
                        products.getAllData(){
                            
                        }
                    }
                }
                
                Button(action: {
                    self.showShipping.toggle()
                }, label: {
                    Text("الشراء")
                        .MonadiFont(style: .white, weight: .black, size: 22)
                        .chooseFont(fontManager: .BalooBhaijaan, style: .white, weight: .black, size: 22)
                        .multilineTextAlignment(.center)
                    
                }).fullScreenCover(isPresented: self.$showShipping, content: {
                    addressShipping()
                })
                .padding()
                .cornerRadius(10)
                .background(colorPallet.move)
                .frame(width: 400, alignment: .center)
                
            }
            
            
        }
        .padding(.top, -100)
        .padding(.bottom, 10)
        
    }
    
    func getIndex(product: ProductListCoreModel)->Int{
        return products.list
            .firstIndex { (product1) -> Bool in
            return product.id == product1.id
        } ?? 0
    }
}

