//
//  about.swift
//  Jeet
//
//  Created by Mac on 6/2/21.
//

import SwiftUI

struct about: View {
    @Environment(\.presentationMode) var presentationMode
    var body: some View {
        VStack{
            HStack{
                Spacer()
                Text("نبذه عنا")
                Spacer()
                Button(action: {
                    presentationMode.wrappedValue.dismiss()
                }, label: {
                    Image(systemName: "arrow.backward")
                        .resizable()
                        .frame(width: 20, height: 20)
                        .foregroundColor(colorPallet.gray)
                        .environment(\.layoutDirection, .leftToRight)
                })
                .padding(.trailing, 10)
            }
            .padding()
            .background(colorPallet.bgCard)
            .shadow(radius: 1)
            
            Spacer()
            
            
            ScrollView{
                
                VStack(spacing: 20){
                    Text("سياسة استرجاع السلع")
                        .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .black, size: 20)
                    
                    Text("هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ولذلك يتم استخدام طريقة لوريم إيبسوم لأنها تعطي توزيعاَ طبيعياَ -إلى حد ما- للأحرف عوضاً ")
                        .chooseFont(fontManager: .BalooBhaijaan, style: .gray, weight: .regular, size: 15)
                        .lineSpacing(10)
                        .multilineTextAlignment(.leading)
                }
                Divider()
                VStack(spacing: 20){
                    Text("كلمات السر")
                        .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .black, size: 20)
                    
                    Text("بإنشاء حساب بائع صحيح لكل دولة من الدول المختارة، فإنكم توافقون في وقت إنشاء الحساب أو التسجيل على الالتزام والتقيد بشروط الموقع هذه وكذلك جميع السياسات والبروتوكولات والإرشادات وغير ذلك من الشروط الواردة في الموقع (والتي تشكل جزءاً لا يتجزأ من شروط الموقع هذه). ويشمل ذلك")
                        .chooseFont(fontManager: .BalooBhaijaan, style: .gray, weight: .regular, size: 15)
                        .lineSpacing(10)
                        .multilineTextAlignment(.leading)
                }
            }.padding()
        }
    }
}

struct about_Previews: PreviewProvider {
    static var previews: some View {
        about()
    }
}
