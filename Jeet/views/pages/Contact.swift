//
//  Contact.swift
//  Jeet
//
//  Created by Mac on 6/2/21.
//

import SwiftUI

struct Contact: View {
    @Environment(\.presentationMode) var presentationMode
    @State var memberName = ""
    @State var memberEmail = ""
    @State var Message = ""
    var body: some View {
        VStack{
            HStack{
                Spacer()
                Text("اتصل بنا")
                Spacer()
                Button(action: {
                    presentationMode.wrappedValue.dismiss()
                }, label: {
                    Image(systemName: "arrow.backward")
                        .resizable()
                        .frame(width: 20, height: 20)
                        .foregroundColor(colorPallet.gray)
                        .environment(\.layoutDirection, .leftToRight)
                })
                .padding(.trailing, 10)
            }
            .padding()
            .background(colorPallet.bgCard)
            .shadow(radius: 1)
            
            Spacer()
            
            
            ScrollView{
                VStack{
                    HStack{
                        TextField("اكتب الاسم هنا", text: $memberName)
                            .padding(.leading, 10)
                            .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 12)
                    }
                    .padding(.vertical, 10)
                    .padding(.horizontal, 20)
                    .padding(.leading, -15)
                    .background(colorPallet.bgCard)
                    .border(colorPallet.move, width: 2)
                    .cornerRadius(6)
                    .padding(.horizontal)
                    .overlay(
                        HStack{
                            Spacer()
                            Image(systemName: "person.fill")
                            
                        }.padding(.horizontal, 20)
                        .foregroundColor(colorPallet.move)
                    )
                    HStack{
                        TextField("اكتب الايميل هنا", text: $memberEmail)
                            .padding(.leading, 10)
                            .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 12)
                    }
                    .padding(.vertical, 10)
                    .padding(.horizontal, 20)
                    .padding(.leading, -15)
                    .background(colorPallet.bgCard)
                    .border(colorPallet.move, width: 2)
                    .cornerRadius(6)
                    .padding(.horizontal)
                    .overlay(
                        HStack{
                            Spacer()
                            Image(systemName: "envelope.fill")
                            
                        }.padding(.horizontal, 20)
                        .foregroundColor(colorPallet.move)
                    )
                    TextEditor(text: $Message)
                        .padding(.vertical, 10)
                        .padding(.horizontal, 20)
                        .padding(.leading, -15)
                        .frame(height: 200, alignment: .top)
                        .background(colorPallet.bgCard)
                        .border(colorPallet.move, width: 2)
                        .cornerRadius(6)
                        .padding(.horizontal)
                    Button(action: {}, label: {
                        Text("إرسال")
                            .chooseFont(fontManager: .BalooBhaijaan, style: .white, weight: .medium, size: 12)
                    })
                    .padding()
                    .padding(.horizontal, 50)
                    .background(colorPallet.move)
                    .foregroundColor(colorPallet.white)
                    .cornerRadius(10)
                }
                
            }.padding()
        }
    }
}

struct Contact_Previews: PreviewProvider {
    static var previews: some View {
        Contact()
    }
}
