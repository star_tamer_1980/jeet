//
//  favouriteList.swift
//  Jeet
//
//  Created by Mac on 7/31/21.
//

import SwiftUI

struct favouriteList: View {
    @State var navigated = false
    @State var productDetailsId = 0
    
    @State var showActionSheet = false
    @ObservedObject var settings = UserSettings()
    @ObservedObject var favouriteList = favouriteObservable()
//    @ObservedObject var productList = productsRatingListObserval()
    @Environment(\.presentationMode) var presentationMode
    
    var body: some View {
        VStack{
            HStack{
                Spacer()
                Text("الاعلي تقييماً")
                    .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 20)
                Spacer()
                Button(action: {
                    presentationMode.wrappedValue.dismiss()
                }, label: {
                    Image(systemName: "arrow.backward")
                        .resizable()
                        .frame(width: 20, height: 20)
                        .foregroundColor(colorPallet.gray)
                        .environment(\.layoutDirection, .leftToRight)
                })
                .padding(.trailing, 10)
            }
            .padding()
            .background(colorPallet.bgCard)
            .shadow(radius: 1)
            
            Spacer()
            
            
            ScrollView{
                LazyVGrid(columns: [
                    GridItem(.flexible(minimum: 150, maximum: 500)),
                    GridItem(.flexible(minimum: 150, maximum: 500))
                ]) {
                    ForEach(favouriteList.listFav, id: \.self){ product in
                        Button(action: {
                            self.navigated.toggle()
                            Print("id press is : \(String(describing: product.id))")
                            self.productDetailsId = product.id ?? 0
                        }, label: {
                            Print("count \(favouriteList.listFav.count - 1)")
                            productCard(product: product, isLast: true, isLoadMore: false)
                                   
                                
                        }).fullScreenCover(isPresented: self.$navigated, content: {
                            Print("id test: \(String(describing: self.productDetailsId))")
                            productDetails(id: self.$productDetailsId)
                        })
                    }


                }.padding(.horizontal, 12)
            }.padding()
        }.background(colorPallet.white)
        .onAppear(){
            DispatchQueue.main.async {
                favouriteList.listFavourite(settings.token)
//                produc1tList.getList(limit: self.limit, paged: self.paged, order: self.order, orderby: self.orderBy, category_id: self.category_id, taxonomy: self.taxonomy, searchWord: self.searchWord)
            }
        }
        
    }
}

struct favouriteList_Previews: PreviewProvider {
    static var previews: some View {
        favouriteList()
    }
}
