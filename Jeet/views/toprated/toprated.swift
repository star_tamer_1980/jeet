//
//  toprated.swift
//  Jeet
//
//  Created by Mac on 6/2/21.
//

import SwiftUI

struct toprated: View {
    @State var searchWord = ""
    @State var isSearching = false
    
    
    @State var navigated = false
    @State var productDetailsId = 0
    
    @State var showActionSheet = false
    
    @State var limit = 10
    @State var paged = 1
    @State var order = "desc"
    @State var orderBy = "date"
    @State var category_id = 0
    @State var taxonomy = "product_cat"
    @ObservedObject var productList = productsRatingListObserval()
    @Environment(\.presentationMode) var presentationMode
    
    var body: some View {
        VStack{
            HStack{
                Spacer()
                Text("الاعلي تقييماً")
                    .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 20)
                Spacer()
                Button(action: {
                    presentationMode.wrappedValue.dismiss()
                }, label: {
                    Image(systemName: "arrow.backward")
                        .resizable()
                        .frame(width: 20, height: 20)
                        .foregroundColor(colorPallet.gray)
                        .environment(\.layoutDirection, .leftToRight)
                })
                .padding(.trailing, 10)
            }
            .padding()
            .background(colorPallet.bgCard)
            .shadow(radius: 1)
            
            Spacer()
            
            
            ScrollView{
                LazyVGrid(columns: [
                    GridItem(.flexible(minimum: 150, maximum: 500)),
                    GridItem(.flexible(minimum: 150, maximum: 500))
                ]) {
                    ForEach(productList.list, id: \.self){ product in
                        Button(action: {
                            self.navigated.toggle()
                            Print("id press is : \(String(describing: product.id))")
                            self.productDetailsId = product.id ?? 0
                        }, label: {
                            Print("count \(productList.list.count - 1)")
                            if (productList.list[productList.list.count - 1].id == product.id){
                                productCard(product: product, isLast: true, isLoadMore: productList.isMore)
                                    .onAppear(){
                                        self.paged = self.paged + 1
                                        DispatchQueue.main.async {
                                            productList.getList(limit: self.limit, paged: self.paged, order: self.order, orderby: self.orderBy, category_id: self.category_id, taxonomy: self.taxonomy, searchWord: self.searchWord)
                                        }
                                    }
                            }else{
                                productCard(product: product, isLast: false)
                            }
                            
                                
                        }).fullScreenCover(isPresented: self.$navigated, content: {
                            Print("id test: \(String(describing: self.productDetailsId))")
                            productDetails(id: self.$productDetailsId)
                        })
                    }


                }.padding(.horizontal, 12)
            }.padding()
        }.background(colorPallet.white)
        .onAppear(){
            DispatchQueue.main.async {
                productList.getList(limit: self.limit, paged: self.paged, order: self.order, orderby: self.orderBy, category_id: self.category_id, taxonomy: self.taxonomy, searchWord: self.searchWord)
            }
        }
        
    }
}

struct toprated_Previews: PreviewProvider {
    static var previews: some View {
        toprated()
        
    }
}
