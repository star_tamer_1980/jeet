//
//  productDetails.swift
//  Jeet
//
//  Created by Mac on 6/4/21.
//

import SwiftUI
import Kingfisher
import CoreData
import ToastUI

struct productDetails: View {
    
    @Binding var id: Int
    @State private var showProgress = true
    @State private var showToast = false
    @State private var showToastAddCart = false
    @State private var btnFavDisabled = false
    @State private var btnAddToCartDisabled = false
    
    @State private var countProductToCart = ""
    
    @Environment(\.presentationMode) var presentationMode
    @ObservedObject var product = productsDetailsObserval()
    @ObservedObject var favouriteObserved = favouriteObservable()
    @ObservedObject var settings = UserSettings()
    
    let coreDM = CoreDataManager()
    
    
    
    var body: some View {
        
        ZStack{
            
            VStack{
                
                HStack{
                    Spacer()
                    Text(product.productDetails.name ?? "")
                        .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 15)
                        .lineLimit(1)
                    Spacer()
                    Button(action: {
                        presentationMode.wrappedValue.dismiss()
                    }, label: {
                        Image(systemName: "arrow.backward")
                            .resizable()
                            .frame(width: 20, height: 20)
                            .foregroundColor(colorPallet.gray)
                            .environment(\.layoutDirection, .leftToRight)
                    })
                    .padding(.trailing, 10)
                }
                .padding()
                .background(colorPallet.bgCard)
                .shadow(radius: 1)
                
                ScrollView{
                    VStack{
                        KFImage(URL(string: product.productDetails.image ?? ""))
                            .resizable()
                            .scaledToFit()
                        HStack{
                            VStack{
                                HStack{
                                    VStack(alignment: .leading){
                                        Text(product.productDetails.name ?? "")
                                            .frame(width: UIScreen.main.bounds.width / 2)
                                            .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 12)
                                        StarsView(rating: CGFloat(product.productDetails.average_rating ?? Int(0.0)), maxRating: 5)
                                            
                                    }
                                    Spacer()
                                    VStack{
                                        Text(product.productDetails.price ?? "")
                                            .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .black, size: 22)
                                        Text(product.productDetails.regular_price ?? "")
                                            .chooseFont(fontManager: .BalooBhaijaan, style: .gray, weight: .medium, size: 15)
                                    }
                                }
                                HStack{
                                    Image(systemName: "house")
                                    Text(product.productDetails.store_name ?? "")
                                        .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .black, size: 22)
                                    Spacer()
                                }
                                VStack{
                                    VStack{
                                        Text("التوفر")
                                            .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .black, size: 17)
                                            .frame(maxWidth: .infinity, alignment: .leading)
                                        Text("\(product.productDetails.quantity ?? 0) قطع")
                                            .chooseFont(fontManager: .BalooBhaijaan, style: .gray, weight: .medium, size: 15)
                                            .frame(maxWidth: .infinity, alignment: .leading)
                                            .padding(.leading, 30)
                                            .padding(.top, 3)
                                    }
                                    .padding(5)
                                }
                                VStack{
                                    VStack{
                                        Text("هذا المنتج يمكن شحنه الي")
                                            .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .black, size: 17)
                                            .frame(maxWidth: .infinity, alignment: .leading)
                                        Text(product.productDetails.ship_to_title ?? "")
                                            .chooseFont(fontManager: .BalooBhaijaan, style: .gray, weight: .medium, size: 15)
                                            .frame(maxWidth: .infinity, alignment: .leading)
                                            .padding(.leading, 30)
                                            .padding(.top, 3)
                                    }
                                    .padding(5)
                                }
                                
                                if(btnAddToCartDisabled == false){
                                    HStack{
                                        TextField("1", text: $countProductToCart)
                                            .multilineTextAlignment(.center)
                                            .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 18)
                                            .overlay(RoundedRectangle(cornerRadius: 10).stroke(colorPallet.move, lineWidth: 1))
                                    }
                                    .background(colorPallet.bgTextField)
                                    .cornerRadius(6)
                                    .padding(.horizontal, 150)
                                    
                                    VStack{
                                        
                                        Button(action: {
                                            coreDM.addProduct(product: product.productDetails, qty: Int(countProductToCart)!)
                                            self.showToastAddCart = true
                                            btnAddToCartDisabled.toggle()
                                        }, label: {
                                            Text("إضافة إلي سلة التسوق")
                                                .MonadiFont(style: .white, weight: .black, size: 20)
                                                .padding(.bottom, 7)
                                        })
                                        .disabled(btnAddToCartDisabled)
                                        .padding()
                                        .toast(isPresented: $showToastAddCart, dismissAfter: 3){
//                                            self.showToastAddCart = false
                                        } content: {
                                            ToastView("تم الاضافه الي سلة التسوق")
                                        }
                                    }
                                    .background(colorPallet.orange)
                                    .cornerRadius(10)
                                    .shadow(color: colorPallet.black.opacity(0.3), radius: 3, x: 3, y: 3)
                                    
                                }else{
                                    HStack{
                                        TextField("1", text: $countProductToCart)
                                            .disabled(btnAddToCartDisabled)
                                            .multilineTextAlignment(.center)
                                            .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .medium, size: 18)
                                            .overlay(RoundedRectangle(cornerRadius: 10).stroke(colorPallet.move, lineWidth: 1))
                                    }
                                    .background(colorPallet.bgTextField)
                                    .cornerRadius(6)
                                    .padding(.horizontal, 150)
                                    
                                    VStack{
                                        
                                        Button(action: {
                                            coreDM.addProduct(product: product.productDetails, qty: Int(countProductToCart)!)
                                            self.showToastAddCart = true
                                            btnAddToCartDisabled.toggle()
                                        }, label: {
                                            VStack(alignment: .center){
                                                Text("إضافة إلي سلة التسوق")
                                                    .MonadiFont(style: .white, weight: .black, size: 20)
                                                    .padding(.bottom, 7)
                                                if(settings.isLogin == false){
                                                    Text("يجب تسجيل الدخول اولاً")
                                                        .MonadiFont(style: .white, weight: .black, size: 10)
                                                }
                                            }
                                            
                                        })
                                        .disabled(btnAddToCartDisabled)
                                        .padding()
                                        .toast(isPresented: $showToastAddCart, dismissAfter: 3){
//                                            self.showToastAddCart = false
                                        } content: {
                                            ToastView("تم الاضافه الي سلة التسوق")
                                        }
                                    }
                                    .background(colorPallet.gray)
                                    .cornerRadius(10)
                                    .shadow(color: colorPallet.black.opacity(0.3), radius: 3, x: 3, y: 3)
                                    
                                }
                                
                                if(btnFavDisabled == false){
                                    VStack{
                                        
                                        Button(action: {
                                            favouriteObserved.addToFavourite(settings.token, product.productDetails.id!)
                                            self.showToast = true
                                            btnFavDisabled.toggle()
                                        }, label: {
                                            Text("إضافة إلي المفضله")
                                                .MonadiFont(style: .white, weight: .black, size: 20)
                                                .padding(.bottom, 7)
                                        })
                                        .padding()
                                        .disabled(btnFavDisabled)
                                        .toast(isPresented: $showToast, dismissAfter: 3){
                                            
                                        } content: {
                                            ToastView("تم الاضافه للمفضله")
                                        }
                                    }
                                    .background(colorPallet.orange)
                                    .cornerRadius(10)
                                    .shadow(color: colorPallet.black.opacity(0.3), radius: 3, x: 3, y: 3)
                                
                                }else{
                                    VStack{
                                        
                                        Button(action: {
                                            
                                        }, label: {
                                            VStack(alignment: .center){
                                                Text("إضافه إلي المفضله")
                                                    .MonadiFont(style: .white, weight: .black, size: 20)
                                                    .padding(.bottom, 7)
                                                if(settings.isLogin == false){
                                                    Text("يجب تسجيل الدخول اولاً")
                                                        .MonadiFont(style: .white, weight: .black, size: 10)
                                                }
                                            }
                                        })
                                        .padding()
                                        .disabled(btnFavDisabled)
                                        .toast(isPresented: $showToast, dismissAfter: 3){
                                            
                                        } content: {
                                            ToastView("تم الاضافه للمفضله")
                                        }
                                        
                                    }
                                    .background(colorPallet.gray)
                                    .cornerRadius(10)
                                    .shadow(color: colorPallet.black.opacity(0.3), radius: 3, x: 3, y: 3)
                                
                                }
                                    
                                
                                VStack{
                                    VStack(alignment: .center, spacing: 30){
                                        Text("المواصفات")
                                            .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .black, size: 22)
                                            .frame(maxWidth: .infinity)
                                            .frame(height: 50)
                                            
                                    }
                                    .background(colorPallet.move)
                                    .border(colorPallet.gray, width: 2)
                                    .shadow(radius: 10)
                                    .padding(.top, 20)
                                    
                                    VStack{
                                        Print("attr \(product.productDetails.id)")
                                        ForEach(product.productDetails.attributes ?? [], id: \.self){ attribut in
                                            VStack{
                                                Text(attribut.name ?? "")
                                                    .chooseFont(fontManager: .BalooBhaijaan, style: .black, weight: .black, size: 17)
                                                    .frame(maxWidth: .infinity, alignment: .leading)
                                                Text(attribut.values[0] ?? "")
                                                    .chooseFont(fontManager: .BalooBhaijaan, style: .gray, weight: .medium, size: 15)
                                                    .frame(maxWidth: .infinity, alignment: .leading)
                                                    .padding(.leading, 30)
                                                    .padding(.top, 3)
                                            }
                                            .padding(5)
                                        }
                                    }
                                }
                                VStack{
                                    VStack(alignment: .center, spacing: 30){
                                        Text("المراجعات")
                                            .MonadiFont(style: colorManager.black, weight: .black, size: 22)
                                            .frame(maxWidth: .infinity)
                                            .frame(height: 50)
                                            
                                    }
                                    .background(colorPallet.move)
                                    .border(colorPallet.gray, width: 2)
                                    .shadow(radius: 10)
                                    .padding(.top, 20)
                                    
                                    VStack{
                                        
                                    }
                                }
                                
                            }
                        }
                    }
                    
                }.padding()
                .overlay(
                    showProgress ? progress() : nil
                )
            }
            .onAppear(){
                if(settings.isLogin == false){
                    btnFavDisabled = true
                    btnAddToCartDisabled = true
                }
                DispatchQueue.main.async {
                    self.product.getData(ID: id){
                        showProgress = false
                        
                    }
                }
                
            }
//            VStack{
//                Spacer()
//                HStack{
//                    Spacer()
//                    Button(action: {
//                        coreDM.addProduct(product: product.productDetails, qty: 1)
//                    }, label: {
//                        Text("+")
//                            .MonadiFont(style: .white, weight: .black, size: 50)
//                            .padding(.bottom, 7)
//                            .frame(width: 77, height: 70)
//                    })
//                    .background(colorPallet.move)
//                    .cornerRadius(38.5)
//                    .padding()
//                    .shadow(color: colorPallet.black.opacity(0.3), radius: 3, x: 3, y: 3)
//                }
//            }
        }
        
        
    }
}
