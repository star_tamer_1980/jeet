//
//  categoryList.swift
//  Jeet
//
//  Created by Mac on 6/2/21.
//

import SwiftUI

struct categoryList: View {
    @State var limit = 12
    @State var paged = 1
    @State var order = "desc"
    @State var orderBy = "date"
    @State var product_id = 0
    @State var taxonomy = "product_cat"
    
    @State var title = ""
    @State var navigated = false
    @State var catDetailsId = 0
    @State var catDetailsTitle = ""
    @State var showProgress = true
    
    @ObservedObject var catList = catListObserval()
    @Environment(\.presentationMode) var presentationMode
    var body: some View {
        VStack{
            HStack{
                Spacer()
                if(self.taxonomy == "product_cat"){
                    Text("التصنيفات")
                }else{
                    Text("البراندات")
                }
                
                Spacer()
                Button(action: {
                    presentationMode.wrappedValue.dismiss()
                }, label: {
                    Image(systemName: "arrow.backward")
                        .resizable()
                        .frame(width: 20, height: 20)
                        .foregroundColor(colorPallet.gray)
                        .environment(\.layoutDirection, .leftToRight)
                })
                .padding(.trailing, 10)
            }
            .padding()
            .background(colorPallet.bgCard)
            .shadow(radius: 1)
            
            Spacer()
            
            
            ScrollView{
                
                LazyVGrid(columns: [
                    GridItem(.flexible(minimum: 50, maximum: 100)),
                    GridItem(.flexible(minimum: 50, maximum: 100)),
                    GridItem(.flexible(minimum: 50, maximum: 100))
                ]) {
                    ForEach(catList.list, id: \.self){ cat in
                        
                        
                        Button(action: {
                            self.navigated.toggle()
                            Print("id press is : \(String(describing: cat.id))")
                            self.catDetailsId = cat.id ?? 0
                            self.catDetailsTitle = cat.name ?? ""
                        }, label: {
                            Print("count \(catList.list.count - 1)")
                            if(self.taxonomy == "product_cat"){
                                Print("count \(catList.list.count - 1)")
                                if (catList.list[catList.list.count - 1].id == cat.id){
                                    categoryCard(row: cat, isLast: true)
                                        .onAppear(){
                                            self.paged = self.paged + 1
                                            DispatchQueue.main.async {
                                                catList.getData(limit: self.limit, paged: self.paged, search: "", product_id: self.product_id, taxonomy: self.taxonomy)
                                            }
                                        }
                                }else{
                                    categoryCard(row: cat)
                                }
                            }else{
                                Print("count \(catList.list.count - 1)")
                                if (catList.list[catList.list.count - 1].id == cat.id){
                                    brandCard(row: cat, isLast: true)
                                        .onAppear(){
                                            self.paged = self.paged + 1
                                            DispatchQueue.main.async {
                                                catList.getData(limit: self.limit, paged: self.paged, search: "", product_id: self.product_id, taxonomy: self.taxonomy)
                                            }
                                        }
                                }else{
                                    brandCard(row: cat)
                                }
                            }
                            
                                
                        }).fullScreenCover(isPresented: self.$navigated, content: {
                            Print("id test: \(String(describing: self.catDetailsId))")
                            latestProducts(titleView: self.catDetailsTitle, category_id: self.catDetailsId, taxonomy: self.taxonomy)
                        })
                        
                        
                    }


                }.padding(.horizontal, 12)
            }.padding()
        }
        .background(colorPallet.white)
        .onAppear(){
            DispatchQueue.main.async {
                catList.getData(limit: self.limit, paged: self.paged, taxonomy: self.taxonomy)
            }
            
        }
    }
}

struct categoryList_Previews: PreviewProvider {
    static var previews: some View {
        categoryList()
    }
}
