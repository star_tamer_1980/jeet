//
//  ContentView.swift
//  Jeet
//
//  Created by Mac on 5/14/21.
//

import SwiftUI

struct ContentView: View {
    @ObservedObject var settings = UserSettings()
    var body: some View {
        
        if settings.showOnStart{
            slideStart()
        }else{
            HomeView()
        }
        
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}


